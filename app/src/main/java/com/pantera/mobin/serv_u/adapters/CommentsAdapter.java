package com.pantera.mobin.serv_u.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pantera.mobin.serv_u.R;
import com.pantera.mobin.serv_u.models.Comment;
import com.pantera.mobin.serv_u.utils.util;

import java.util.List;

/**
 * Created by Mobin on 5/24/2017.
 */

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.CommentViewHolder> {
    private List<Comment> list;

    public CommentsAdapter(List<Comment> commentsList) {
        list = commentsList;
    }

    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CommentViewHolder(View.inflate(parent.getContext(), R.layout.comments_adapter, null));
    }

    @Override
    public void onBindViewHolder(CommentViewHolder holder, int position) {
        Comment comment = list.get(position);
        holder.name.setText(comment.getUser().getName());
        holder.time.setText(util.timeFormat(comment.getCreated_at()));
        holder.comment.setText(comment.getBody());
//        Picasso.with(holder.itemView.getContext()).load(comment.getUser().getProfile_pic().getUrl()).transform(new CircleTranform()).into(holder.avatar);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class CommentViewHolder extends RecyclerView.ViewHolder {
        TextView name, time,comment;
        ImageView avatar;


        CommentViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            time = (TextView) itemView.findViewById(R.id.time);
            avatar = (ImageView) itemView.findViewById(R.id.avatar);
            comment = (TextView)itemView.findViewById(R.id.comment);

        }
    }
}
