package com.pantera.mobin.serv_u.utils;

import com.pantera.mobin.serv_u.models.Document;

/**
 * Created by Mobin on 5/15/2017.
 */

public class Constant {
    public static final String key_fb_token = "key_fb_token";
    public static final String key_twitter_token = "key_twitter_token";
    public static final String key_google_token = "key_google_token";
    public static final String key_token = "key_token";
    public static final String key_client = "key_client";
    public static final String key_uid = "uid";
    public static final String key_id = "id";
    public static final String key_userType = "userType";
    public static final String key_company ="company";
    public static int userType = -1;
    public static final String key_jobID = "JobID";
    public static Document profile;
    public static String client = "";
    public static String uid = "";
    public static String company = "";
    public static int id = -1;
    public static String token = "";
    public static String fb_token = "";
    public static String twitter_token = "";
    public static String google_token = "";
}
