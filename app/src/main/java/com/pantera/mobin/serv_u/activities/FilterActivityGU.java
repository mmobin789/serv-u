package com.pantera.mobin.serv_u.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.pantera.mobin.serv_u.R;
import com.pantera.mobin.serv_u.interfaces.OnJobListener;
import com.pantera.mobin.serv_u.models.Job;

public class FilterActivityGU extends BaseActivity implements View.OnClickListener, OnJobListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_gu);
        initViews();
    }

    @Override
    void initViews() {
        setToolBar();
    }

    private void setToolBar() {
        showToolBar(getString(R.string.filter), false);
        TextView cancel = (TextView) findViewById(R.id.cancel);
        TextView done = (TextView) findViewById(R.id.editTxt);
        cancel.setVisibility(View.VISIBLE);
        done.setVisibility(View.VISIBLE);
        done.setText(getString(R.string.done));
        done.setOnClickListener(this);
        cancel.setOnClickListener(this);

    }

    @Override
    void setListeners() {

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.cancel:
                onBackPressed();
                break;

            case R.id.editTxt:
                break;
        }

    }



    @Override
    public void OnJobAdded(Job job) {

    }

    @Override
    public void OnJobFailed(String error) {

    }
}
