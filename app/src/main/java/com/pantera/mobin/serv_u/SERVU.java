package com.pantera.mobin.serv_u;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.pantera.mobin.serv_u.enums.LoginType;
import com.pantera.mobin.serv_u.models.Auth;
import com.pantera.mobin.serv_u.utils.Constant;

import static com.pantera.mobin.serv_u.enums.LoginType.APP;
import static com.pantera.mobin.serv_u.enums.LoginType.FACEBOOK;
import static com.pantera.mobin.serv_u.enums.LoginType.GOOGLE;
import static com.pantera.mobin.serv_u.enums.LoginType.TWITTER;
import static com.pantera.mobin.serv_u.utils.Constant.fb_token;
import static com.pantera.mobin.serv_u.utils.Constant.google_token;
import static com.pantera.mobin.serv_u.utils.Constant.token;
import static com.pantera.mobin.serv_u.utils.Constant.twitter_token;

/**
 * Created by Mobin on 5/15/2017.
 */

public class SERVU extends Application {

    private static SharedPreferences prefs;
    public static LoginType loginType;

    @Override
    public void onCreate() {
        super.onCreate();
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
    }

    public static void clearLoginData() {
        prefs.edit().clear().apply();
    }


    public static void saveLogin(Auth user) {
        SharedPreferences.Editor editor = prefs.edit();
        if (!TextUtils.isEmpty(user.getToken())) {
            editor.putString(Constant.key_token, user.getToken());
            editor.putString(Constant.key_client, user.getClient());
            editor.putString(Constant.key_uid, user.getUid());
            editor.putInt(Constant.key_id, user.getUser().getId());
            editor.putInt(Constant.key_userType, user.getUser().getUser_type());
            editor.putString(Constant.key_company, user.getUser().getCompany_name());
        } else if (!TextUtils.isEmpty(user.getUser().getFbLoginToken())) {
            editor.putString(Constant.key_fb_token, user.getUser().getFbLoginToken());
        } else if (!user.getUser().getTwitterLoginToken().isEmpty()) {
            editor.putString(Constant.key_twitter_token, user.getUser().getTwitterLoginToken());
        } else if (!TextUtils.isEmpty(user.getUser().getGoogleLoginToken())) {
            editor.putString(Constant.key_google_token, user.getUser().getGoogleLoginToken());
        }
        editor.apply();
        hasLoginPrefs();

    }

    @NonNull
    public static Boolean hasLoginPrefs() {
        if (prefs != null) {
            Constant.company = prefs.getString(Constant.key_company, "");
            Constant.userType = prefs.getInt(Constant.key_userType, -1);
            token = prefs.getString(Constant.key_token, "");
            Constant.client = prefs.getString(Constant.key_client, "");
            Constant.uid = prefs.getString(Constant.key_uid, "");
            Constant.id = prefs.getInt(Constant.key_id, -1);
            fb_token = prefs.getString(Constant.key_fb_token, "");
            twitter_token = prefs.getString(Constant.key_twitter_token, "");
            boolean hasLogin = false;

            if (!fb_token.isEmpty()) {
                loginType = FACEBOOK;
                hasLogin = true;
            } else if (!twitter_token.isEmpty()) {
                loginType = TWITTER;
                hasLogin = true;
            } else if (!token.isEmpty()) {
                loginType = APP;
                hasLogin = true;
            } else if (!google_token.isEmpty()) {
                loginType = GOOGLE;
                hasLogin = true;
            }
            return hasLogin;
        } else return false;
    }
}
