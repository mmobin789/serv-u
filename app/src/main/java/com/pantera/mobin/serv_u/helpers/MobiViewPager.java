package com.pantera.mobin.serv_u.helpers;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.pantera.mobin.serv_u.R;

/**
 * Created by Mobin on 6/1/2017.
 */

public class MobiViewPager extends ViewPager {
    private boolean swipeEnabled;

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return swipeEnabled;

    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return swipeEnabled;
    }

    public MobiViewPager(Context context) {
        super(context);
    }

    public MobiViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.MobiViewPager);
        swipeEnabled = typedArray.getBoolean(R.styleable.MobiViewPager_swipeEnabled, true);
        typedArray.recycle();
    }

    public void setSwipeEnabled(boolean swipeEnabled) {
        this.swipeEnabled = swipeEnabled;
    }
}
