package com.pantera.mobin.serv_u.activities;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.pantera.mobin.serv_u.R;
import com.pantera.mobin.serv_u.utils.util;

public abstract class BaseActivity extends AppCompatActivity {


    abstract void initViews();

    abstract void setListeners();

    public void showMsgDialog(String msg) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.msg_dialog);
        TextView textView = (TextView) dialog.findViewById(R.id.msg);
        textView.setText(msg);
        dialog.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    protected void showToolBar(String title, Boolean showBackButton) {
        TextView back = (TextView) findViewById(R.id.back);
        if (showBackButton)
            back.setVisibility(View.VISIBLE);
        else back.setVisibility(View.GONE);
        TextView titleV = (TextView) findViewById(R.id.toolbar_title);
        titleV.setText(title);
        util.setFontAwesome(new View[]{back});
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        findViewById(R.id.right).setVisibility(View.GONE);
        findViewById(R.id.plus).setVisibility(View.GONE);
        findViewById(R.id.editProfile).setVisibility(View.GONE);
        findViewById(R.id.editTxt).setVisibility(View.GONE);
        findViewById(R.id.filter).setVisibility(View.GONE);
        findViewById(R.id.menu).setVisibility(View.GONE);

    }


}
