package com.pantera.mobin.serv_u.models;

/**
 * Created by Mobin on 5/16/2017.
 */

public class Bid {
    public int getId() {
        return id;
    }

    public int getJob_id() {
        return job_id;
    }

    public double getBudget() {
        return budget;
    }

    public String getDescription() {
        return description;
    }

    public String getCreated_at() {
        return created_at;
    }

    public User getUser() {
        return user;
    }

    private int id,job_id;
    private double budget;
    private String description,created_at;
    private User user;

}
