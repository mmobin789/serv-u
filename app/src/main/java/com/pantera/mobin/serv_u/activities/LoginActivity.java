package com.pantera.mobin.serv_u.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.pantera.mobin.serv_u.R;
import com.pantera.mobin.serv_u.SERVU;
import com.pantera.mobin.serv_u.api.ApiSERVU;
import com.pantera.mobin.serv_u.interfaces.OnLoginListener;
import com.pantera.mobin.serv_u.models.Auth;
import com.pantera.mobin.serv_u.models.LoginInput;
import com.pantera.mobin.serv_u.utils.util;

import static com.pantera.mobin.serv_u.utils.util.dismissProgressDialog;
import static com.pantera.mobin.serv_u.utils.util.showProgressDialog;

public class LoginActivity extends BaseActivity implements View.OnClickListener, OnLoginListener {

    TextView googleLogin, fbLogin, twitterLogin, signUp;
    EditText emailPhn, password;
    Button login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initViews();
        setListeners();

    }

    void setSignUp() {

        String signUptxt = signUp.getText().toString();
        SpannableStringBuilder string = util.getBoldText(this, signUptxt, 23, signUptxt.length(), R.color.btn_border);
        string.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                startActivity(new Intent(widget.getContext(), SignUpActivity.class));

            }

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
            }
        }, 23, signUptxt.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        signUp.setText(string);
        signUp.setMovementMethod(LinkMovementMethod.getInstance());

    }

    @Override
    void initViews() {

        googleLogin = (TextView) findViewById(R.id.google);
        fbLogin = (TextView) findViewById(R.id.fb);
        twitterLogin = (TextView) findViewById(R.id.twitter);
        signUp = (TextView) findViewById(R.id.signUp);
        login = (Button) findViewById(R.id.login);
        emailPhn = (EditText) findViewById(R.id.emlPhn);
        password = (EditText) findViewById(R.id.password);

        util.setFontAwesome(new View[]{fbLogin, googleLogin, twitterLogin});
        setSignUp();

    }

    @Override
    void setListeners() {
        login.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v == login) {
            tryLogin();

        }

    }

    private void tryLogin() {
        String emailPhn = this.emailPhn.getText().toString().trim();
        String password = this.password.getText().toString().trim();
        LoginInput loginInput = new LoginInput();

        if (emailPhn.isEmpty()) {
            this.emailPhn.setError("Email/phone Required");
        } else if (!util.isValidEmail(emailPhn)) {
            this.emailPhn.setError("Invalid Email");
        }
        if (password.isEmpty()) {
            this.password.setError("Password Required");
        } else if (password.length() < 6) {
            this.password.setError(getString(R.string.short_pass));
        }
        if (util.isValidEmail(emailPhn) && password.length() >= 6) {
            showProgressDialog(this, "Logging in with Email...", false);

            loginInput.setEmail(emailPhn);
            loginInput.setPassword(password);
            ApiSERVU.doLogin(this, loginInput);
        } else if (!util.isValidEmail(emailPhn) && password.length() >= 6) {
            showProgressDialog(this, "Logging in with Phone...", false);
            this.emailPhn.setError(null);
            loginInput.setEmail(emailPhn);
            loginInput.setPassword(password);
            ApiSERVU.doLogin(this, loginInput);
        }


    }

    @Override
    public void onLoginSuccess(Auth user) {
        dismissProgressDialog();
        SERVU.saveLogin(user);
        finish();

        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public void onLoginFailed(String error) {
        dismissProgressDialog();
        showMsgDialog(error);
    }



}
