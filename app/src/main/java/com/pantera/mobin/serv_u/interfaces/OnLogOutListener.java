package com.pantera.mobin.serv_u.interfaces;

/**
 * Created by Mobin on 5/19/2017.
 */

public interface OnLogOutListener {

    void OnLogOutSuccess(boolean success);
    void OnLogOutFailed(String error);
}
