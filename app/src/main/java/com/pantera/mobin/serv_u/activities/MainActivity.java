package com.pantera.mobin.serv_u.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.pantera.mobin.serv_u.R;
import com.pantera.mobin.serv_u.SERVU;
import com.pantera.mobin.serv_u.api.ApiSERVU;
import com.pantera.mobin.serv_u.fragments.JobsFragment;
import com.pantera.mobin.serv_u.fragments.MessagesFragment;
import com.pantera.mobin.serv_u.fragments.NotificationsFragment;
import com.pantera.mobin.serv_u.fragments.ProfileFragment;
import com.pantera.mobin.serv_u.fragments.SettingsFragment;
import com.pantera.mobin.serv_u.helpers.FragmentHelper;
import com.pantera.mobin.serv_u.interfaces.OnLogOutListener;
import com.pantera.mobin.serv_u.utils.Constant;
import com.pantera.mobin.serv_u.utils.util;

public class MainActivity extends BaseActivity implements View.OnClickListener, OnLogOutListener {

    ImageView msgs, profile, settings, notifications, jobs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        setListeners();

    }

    @Override
    void initViews() {
        msgs = (ImageView) findViewById(R.id.msgs);
        profile = (ImageView) findViewById(R.id.profile);
        settings = (ImageView) findViewById(R.id.settings);
        jobs = (ImageView) findViewById(R.id.myjobs);
        notifications = (ImageView) findViewById(R.id.notif);


    }

    @Override
    void setListeners() {
        msgs.setOnClickListener(this);
        profile.setOnClickListener(this);
        notifications.setOnClickListener(this);
        jobs.setOnClickListener(this);
        settings.setOnClickListener(this);
        // if(Constant.userType==)
        jobs.performClick();
    }

    public void settingsToolbar() {
        showToolBar(getString(R.string.settings), false);
    }

    public void jobsToolbar() {
        showToolBar(getString(R.string.jobs), false);
        ImageView filter = (ImageView) findViewById(R.id.filter);
        filter.setVisibility(View.VISIBLE);
        filter.setOnClickListener(this);
        if (Constant.userType == 1) { // gu
            ImageView addJob = (ImageView) findViewById(R.id.right);
            addJob.setVisibility(View.VISIBLE);
            addJob.setOnClickListener(this);
        } else if (Constant.userType == 2 && !Constant.company.isEmpty()) { //sp
            TextView textView = (TextView) findViewById(R.id.editTxt);
            textView.setText(getString(R.string.my_jobs));
            textView.setVisibility(View.VISIBLE);
            textView.setOnClickListener(this);
        }


    }

    public void notificationsToolbar() {
        showToolBar(getString(R.string.notif), false);


    }

    public void profileToolBar() {
        showToolBar(getString(R.string.profile), false);
        ImageView imageView = (ImageView) findViewById(R.id.editProfile);
        imageView.setVisibility(View.VISIBLE);
        TextView menu = (TextView) findViewById(R.id.menu);
        menu.setVisibility(View.VISIBLE);
        util.setFontAwesome(new View[]{menu});
        menu.setOnClickListener(this);


    }

    public void messagesToolbar() {
        showToolBar(getString(R.string.messages), false);
        ImageView imageView = (ImageView) findViewById(R.id.right);
        imageView.setVisibility(View.VISIBLE);
        imageView.setImageResource(R.drawable.pencil);
    }

    private void showPopup(View v) {
        PopupMenu popup = new PopupMenu(v.getContext(), v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.signout, popup.getMenu());
        popup.show();
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.signOut:
                        ApiSERVU.logOut(MainActivity.this, MainActivity.this);
                        break;
                }

                return true;
            }
        });
    }

//    public void hideErrorLayout() {
//        TextView textView = (TextView) findViewById(R.id.error);
//        textView.setVisibility(View.GONE);
//    }

    public void showErrorLayout(String error) {
        if (!util.isInternetAvailable(this) || TextUtils.isEmpty(error)) {
            error = "No Internet Found";
        }
        TextView textView = (TextView) findViewById(R.id.error);
        textView.setVisibility(View.VISIBLE);
        textView.setText(error);


    }


    @Override
    public void onClick(View v) {
        Fragment fragment = null;
        if (v.getId() == R.id.filter) {
            Intent filter = new Intent();
            if (Constant.userType == 2)
                filter.setClass(v.getContext(), FilterActivitySP.class);
            else if (Constant.userType == 1) {
                filter.setClass(v.getContext(), FilterActivityGU.class);
            }
            startActivity(filter);
        }
        if (v.getId() == R.id.menu) {
            showPopup(v);
        }
        if (v.getId() == R.id.right) {
            startActivity(new Intent(v.getContext(), NewJobActivity.class));
        }
        if (v.getId() == jobs.getId()) {
            fragment = JobsFragment.newInstance();
            jobs.setEnabled(false);
            msgs.setEnabled(true);
            notifications.setEnabled(true);
            profile.setEnabled(true);
            settings.setEnabled(true);
        }
        if (v.getId() == msgs.getId()) {
            fragment = MessagesFragment.newInstance();
            jobs.setEnabled(true);
            msgs.setEnabled(false);
            notifications.setEnabled(true);
            profile.setEnabled(true);
            settings.setEnabled(true);
        }
        if (v.getId() == notifications.getId()) {
            fragment = NotificationsFragment.newInstance();
            jobs.setEnabled(true);
            msgs.setEnabled(true);
            notifications.setEnabled(false);
            profile.setEnabled(true);
            settings.setEnabled(true);
        }
        if (v.getId() == profile.getId()) {
            fragment = ProfileFragment.newInstance();
            jobs.setEnabled(true);
            msgs.setEnabled(true);
            notifications.setEnabled(true);
            profile.setEnabled(false);
            settings.setEnabled(true);
        }
        if (v.getId() == settings.getId()) {
            fragment = SettingsFragment.newInstance();
            jobs.setEnabled(true);
            msgs.setEnabled(true);
            notifications.setEnabled(true);
            profile.setEnabled(true);
            settings.setEnabled(false);
        }
        if (fragment != null)
            FragmentHelper.loadFragment(this, fragment);


    }

    @Override
    public void OnLogOutSuccess(boolean success) {

        if (success) {
            SERVU.clearLoginData();
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

    }

    @Override
    public void OnLogOutFailed(String error) {
        showErrorLayout(error);
    }
}
