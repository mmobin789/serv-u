package com.pantera.mobin.serv_u.api;

import com.pantera.mobin.serv_u.interfaces.API;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Mobin on 5/10/2017.
 */

public class RestClient {
    private static String base_url_test = "http://panterasrv.cloudapp.net/servu_api_test/api/";
    private static String base_url = base_url_test;
    private static Retrofit retrofit;
    private static API api;

    public static API getAPI() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(base_url)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        if (api == null) {
            api = retrofit.create(API.class);
        }
        return api;
    }
}
