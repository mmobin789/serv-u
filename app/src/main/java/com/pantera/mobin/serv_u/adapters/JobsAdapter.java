package com.pantera.mobin.serv_u.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pantera.mobin.serv_u.R;
import com.pantera.mobin.serv_u.interfaces.OnJobClickedListener;
import com.pantera.mobin.serv_u.models.Job;
import com.pantera.mobin.serv_u.utils.util;

import java.util.List;

/**
 * Created by Mobin on 5/16/2017.
 */

public class JobsAdapter extends RecyclerView.Adapter<JobsAdapter.JobViewHolder> {
    private List<Job> list;
    private OnJobClickedListener listener;

    public JobsAdapter(OnJobClickedListener listener, List<Job> jobs) {
        list = jobs;
        this.listener = listener;

    }

    @Override
    public JobViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new JobViewHolder(View.inflate(parent.getContext(), R.layout.jobs_adapter, null));
    }

    @Override
    public void onBindViewHolder(final JobViewHolder holder, final int position) {

        final Job jobs = list.get(position);
        holder.userName.setText(jobs.getUser().getName());
        holder.jobTitle.setText(jobs.getTitle());
        holder.jobLoc.setText(jobs.getLocation().getArea());
        holder.category.setText(jobs.getCategory().getTitle());
        holder.status.setImageResource(getJobStatus(jobs.getStatus()));
        holder.time.setText(util.dateFormat(jobs.getCreated_at()) + "\t" + "");


//        Picasso.with(holder.itemView.getContext()).load(jobs.getImages().get(position).getUrl()).transform(new CircleTranform()).into(holder.jobPicBig);
        //       Picasso.with(holder.itemView.getContext()).load(Constant.profile.getUrl()).transform(new CircleTranform()).into(holder.userPic);
    }

    private int getJobStatus(int status) {

        int res = -1;
        if (status == 1)
            res = R.drawable.open_timeline;
        if (status == 2)
            res = R.drawable.awarded_timeline;
        if (status == 3)
            res = R.drawable.ongoing_timeline;
        if (status == 4)
            res = R.drawable.completed_timeline;

        return res;

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class JobViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView userName, jobLoc, jobTitle, category, time;
        ImageView userPic, jobPicBig, status;
        RelativeLayout rlmain;

        JobViewHolder(View itemView) {
            super(itemView);

            rlmain = (RelativeLayout) itemView.findViewById(R.id.rlJobViewHolder_main);
            userName = (TextView) itemView.findViewById(R.id.name);
            jobLoc = (TextView) itemView.findViewById(R.id.loc);
            jobTitle = (TextView) itemView.findViewById(R.id.jobTitle);
            category = (TextView) itemView.findViewById(R.id.cat);
            time = (TextView) itemView.findViewById(R.id.time);
            userPic = (ImageView) itemView.findViewById(R.id.userPic);
            jobPicBig = (ImageView) itemView.findViewById(R.id.jobPicBig);
            status = (ImageView) itemView.findViewById(R.id.status);
            rlmain.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.rlJobViewHolder_main) {
                if (listener != null) {
                    listener.OnJobClicked(list.get(getAdapterPosition()), getAdapterPosition());
                }
            }
        }
    }
}
