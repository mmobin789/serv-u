package com.pantera.mobin.serv_u.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.makeramen.roundedimageview.RoundedImageView;
import com.pantera.mobin.serv_u.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Mobin on 5/25/2017.
 */

public class CarouselAdapter extends RecyclerView.Adapter<CarouselAdapter.CarouselViewHolder> {
    //  private List<Document> list;
    private List<Integer> list;

//    public CarouselAdapter(List<Document> documents) {
//
//
//        list = documents;
//    }

    public CarouselAdapter(List<Integer> resIDs) {

        list = resIDs;
    }

    @Override
    public CarouselViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CarouselViewHolder(View.inflate(parent.getContext(), R.layout.carousel_layout, null));
    }

    @Override
    public void onBindViewHolder(CarouselViewHolder holder, int position) {
           // holder.cardView.setCardElevation(position+2);

       // ViewCompat.setTranslationZ(holder.cardView,position+1);
        Picasso.with(holder.itemView.getContext()).load(list.get(position)).into(holder.roundedImageView);
        //   holder.roundedImageView.setImageDrawable();

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class CarouselViewHolder extends RecyclerView.ViewHolder {
        RoundedImageView roundedImageView;
        CardView cardView;

        CarouselViewHolder(View itemView) {
            super(itemView);

            roundedImageView = (RoundedImageView) itemView.findViewById(R.id.carouselImage);
            cardView = (CardView) itemView.findViewById(R.id.card);

        }


    }
}
