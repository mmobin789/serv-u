package com.pantera.mobin.serv_u.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mobin on 5/24/2017.
 */

public class CommentsPage {

    @SerializedName("timestamp")
    private String timeStamp;
    @SerializedName("page_size")
    private int pageSize;

    private boolean more_available;
    private int page;
    private int total_pages;
    private int total_records;

    public String getTimeStamp() {
        return timeStamp;
    }

    public int getPageSize() {
        return pageSize;
    }

    public boolean isMore_available() {
        return more_available;
    }

    public int getPage() {
        return page;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public int getTotal_records() {
        return total_records;
    }

    public List<Comment> getComments() {
        return comments;
    }

    private List<Comment> comments;
}
