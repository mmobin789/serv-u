package com.pantera.mobin.serv_u.models;

import java.util.List;

/**
 * Created by Mobin on 5/10/2017.
 */

public class User {
    private String name; // user_id for notifications
    private int id;
    private int status, user_type, location_id, total_review_count, total_skill_count, total_jobs_created, total_jobs_completed;
    private float average_rating;

    public float getAverage_rating() {
        return average_rating;
    }

    private String email;
    private String phone;
    private String password;
    private String about, company_name;
    private String token = "", created_at;


    public int getTotal_review_count() {
        return total_review_count;
    }

    public int getTotal_skill_count() {
        return total_skill_count;
    }

    public int getTotal_jobs_created() {
        return total_jobs_created;
    }

    public int getTotal_jobs_completed() {
        return total_jobs_completed;
    }

    public String getCompany_name() {
        return company_name;
    }

    public String getCreated_at() {
        return created_at;
    }

    public Document getProfile_pic() {
        return profile_pic;
    }

    public List<Skill> getSkills() {
        return skills;
    }

    private Document profile_pic;
    private List<Skill> skills;

    public int getId() {
        return id;
    }

    public int getStatus() {
        return status;
    }

    public int getUser_type() {
        return user_type;
    }

    public int getLocation_id() {
        return location_id;
    }

    private String client;

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getUid() {
        return uid;
    }


    private String uid;
    private String fbLoginToken;
    private String twitterLoginToken;

    public String getGoogleLoginToken() {
        return googleLoginToken;
    }

    public void setGoogleLoginToken(String googleLoginToken) {
        this.googleLoginToken = googleLoginToken;
    }

    private String googleLoginToken;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getFbLoginToken() {
        return fbLoginToken;
    }

    public void setFbLoginToken(String fbLoginToken) {
        this.fbLoginToken = fbLoginToken;
    }

    public String getTwitterLoginToken() {
        return twitterLoginToken;
    }

    public void setTwitterLoginToken(String twitterLoginToken) {
        this.twitterLoginToken = twitterLoginToken;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
