package com.pantera.mobin.serv_u.models;

import java.util.List;

/**
 * Created by Mobin on 5/18/2017.
 */

public class Notifications {

    private String timestamp;
    private int page_size, page, total_pages, total_records;
    private boolean more_available;
    private List<Notification> notifications;

    public String getTimestamp() {
        return timestamp;
    }

    public int getPage_size() {
        return page_size;
    }

    public int getPage() {
        return page;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public int getTotal_records() {
        return total_records;
    }

    public boolean isMore_available() {
        return more_available;
    }

    public List<Notification> getNotifications() {
        return notifications;
    }
}
