package com.pantera.mobin.serv_u.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Mobin on 5/10/2017.
 */

public class util {
    private static ProgressDialog progressDialog;


    private static final String PATH_CACHE = Environment.getExternalStorageDirectory() + "/" + "SERV-U" + "/cache/";

    public static boolean isInternetAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void dismissProgressDialog() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    public static String dateFormat(String timeStamp) {
        // parse format is specially created for this servers timestamp

        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
        try {
            date = sdf.parse(timeStamp);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date != null) {
            sdf = new SimpleDateFormat("d MMM yyyy K:mm a", Locale.ENGLISH);
            return sdf.format(date);
        } else return "";
    }

    public static String timeFormat(String timeStamp) {
        // parse format is specially created for this servers timestamp

        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
        try {
            date = sdf.parse(timeStamp);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date != null) {
            sdf = new SimpleDateFormat("K:mm a", Locale.ENGLISH);
            return sdf.format(date);
        } else return "";
    }

    public static String encodeImageToBase64(String absolutePath) {
        Bitmap bm;
        bm = BitmapFactory.decodeFile(absolutePath);
        //   Log.v(TAG, "converting image to bitmap");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
        byte[] byteArrayImage = baos.toByteArray();
        //    Log.i(TAG, "encoded Image to base64");
        return Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
    }

    public static enum ScalingLogic {
        CROP, FIT
    }

    public static Bitmap decodeFile(String path, int dstWidth, int dstHeight,
                                    ScalingLogic scalingLogic) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        options.inJustDecodeBounds = false;
        options.inSampleSize = calculateSampleSize(options.outWidth, options.outHeight, dstWidth,
                dstHeight, scalingLogic);
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(path);
        } catch (FileNotFoundException e) {
            //  Log.e(TAG, e.toString());
        }
        return BitmapFactory.decodeStream(fis, null, options);
    }

    public static int calculateSampleSize(int srcWidth, int srcHeight, int dstWidth, int dstHeight,
                                          ScalingLogic scalingLogic) {
        if (scalingLogic == ScalingLogic.FIT) {
            final float srcAspect = (float) srcWidth / (float) srcHeight;
            final float dstAspect = (float) dstWidth / (float) dstHeight;

            if (srcAspect > dstAspect) {
                return srcWidth / dstWidth;
            } else {
                return srcHeight / dstHeight;
            }
        } else {
            final float srcAspect = (float) srcWidth / (float) srcHeight;
            final float dstAspect = (float) dstWidth / (float) dstHeight;

            if (srcAspect > dstAspect) {
                return srcHeight / dstHeight;
            } else {
                return srcWidth / dstWidth;
            }
        }
    }

    public static Rect calculateDstRect(int srcWidth, int srcHeight, int dstWidth, int dstHeight,
                                        ScalingLogic scalingLogic) {
        if (scalingLogic == ScalingLogic.FIT) {
            final float srcAspect = (float) srcWidth / (float) srcHeight;
            final float dstAspect = (float) dstWidth / (float) dstHeight;

            if (srcAspect > dstAspect) {
                return new Rect(0, 0, dstWidth, (int) (dstWidth / srcAspect));
            } else {
                return new Rect(0, 0, (int) (dstHeight * srcAspect), dstHeight);
            }
        } else {
            return new Rect(0, 0, dstWidth, dstHeight);
        }
    }

    public static Rect calculateSrcRect(int srcWidth, int srcHeight, int dstWidth, int dstHeight,
                                        ScalingLogic scalingLogic) {
        if (scalingLogic == ScalingLogic.CROP) {
            final float srcAspect = (float) srcWidth / (float) srcHeight;
            final float dstAspect = (float) dstWidth / (float) dstHeight;

            if (srcAspect > dstAspect) {
                final int srcRectWidth = (int) (srcHeight * dstAspect);
                final int srcRectLeft = (srcWidth - srcRectWidth) / 2;
                return new Rect(srcRectLeft, 0, srcRectLeft + srcRectWidth, srcHeight);
            } else {
                final int srcRectHeight = (int) (srcWidth / dstAspect);
                final int scrRectTop = (int) (srcHeight - srcRectHeight) / 2;
                return new Rect(0, scrRectTop, srcWidth, scrRectTop + srcRectHeight);
            }
        } else {
            return new Rect(0, 0, srcWidth, srcHeight);
        }
    }

    public static Bitmap createScaledBitmap(Bitmap unscaledBitmap, int dstWidth, int dstHeight,
                                            ScalingLogic scalingLogic) {
        Rect srcRect = calculateSrcRect(unscaledBitmap.getWidth(), unscaledBitmap.getHeight(),
                dstWidth, dstHeight, scalingLogic);
        Rect dstRect = calculateDstRect(unscaledBitmap.getWidth(), unscaledBitmap.getHeight(),
                dstWidth, dstHeight, scalingLogic);
        Bitmap scaledBitmap = Bitmap.createBitmap(dstRect.width(), dstRect.height(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(scaledBitmap);
        canvas.drawBitmap(unscaledBitmap, srcRect, dstRect, new Paint(Paint.FILTER_BITMAP_FLAG));

        return scaledBitmap;
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix,
                true);
    }

    public static Bitmap changeRotation(Bitmap bitmap, String photoPath) {
        ExifInterface ei = null;
        Bitmap rotatedBitmap = null;
        try {
            ei = new ExifInterface(photoPath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                rotatedBitmap = rotateImage(bitmap, 90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                rotatedBitmap = rotateImage(bitmap, 180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                rotatedBitmap = rotateImage(bitmap, 270);
                break;
            case ExifInterface.ORIENTATION_NORMAL:
                rotatedBitmap = bitmap;
            default:
                rotatedBitmap = bitmap;
                break;
        }
        return rotatedBitmap;
    }

    public static File decodeFile(String path) {
        String strMyImagePath = null;
        Bitmap scaledBitmap = null;
        File f = new File(path);
        File savedFile = null;
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, options);
            int imageHeight = options.outHeight;
            int imageWidth = options.outWidth;

            // Part 1: Decode image
            if (imageHeight > 800 && imageWidth > 800) {
                scaledBitmap = decodeFile(path, 800, 800, ScalingLogic.FIT);
                if (scaledBitmap != null)
                    if (scaledBitmap.getWidth() > 800 && scaledBitmap.getHeight() > 800) {
                        // Part 2: Scale image
                        scaledBitmap = createScaledBitmap(scaledBitmap, 800, 800, ScalingLogic.FIT);
                    } //else {
                // scaledBitmap.recycle();
                // }
            }
            strMyImagePath = PATH_CACHE + new Date().getTime() + "_" + f.getName();
            savedFile = new File(strMyImagePath);
            savedFile.getParentFile().mkdirs();
            if (!savedFile.getParentFile().exists() && !savedFile.getParentFile().mkdirs()) {
                Log.e("error", "Unable to create " + savedFile.getParentFile());
            }
            savedFile.createNewFile();
            FileOutputStream fos = null;

            try {
                fos = new FileOutputStream(savedFile);
                if (scaledBitmap == null) {
                    options.inSampleSize = 4;
                    options.inJustDecodeBounds = false;
                    scaledBitmap = BitmapFactory.decodeFile(path, options);
                    scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                }
                scaledBitmap = changeRotation(scaledBitmap, path);
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);

                fos.flush();
                fos.close();
            } catch (FileNotFoundException e) {
                //   Log.e(TAG, e.toString());
                return f;
            } catch (Exception e) {
                //     Log.e(TAG, e.toString());
                return f;
            }
        } catch (Exception e) {
            // Log.e(TAG, e.toString());
            return f;
        }

        return savedFile;
    }


    public static File rotateAndSaveImage(String path, Bitmap scaledBitmap) {
        String strMyImagePath = null;
        File f = new File(path);
        File savedFile = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        strMyImagePath = PATH_CACHE + new Date().getTime() + "_" + f.getName();
        savedFile = new File(strMyImagePath);
        savedFile.getParentFile().mkdirs();
        if (!savedFile.getParentFile().exists() && !savedFile.getParentFile().mkdirs()) {
            Log.e("error", "Unable to create " + savedFile.getParentFile());
        }
        try {
            savedFile.createNewFile();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        FileOutputStream fos = null;

        try {
            fos = new FileOutputStream(savedFile);
            if (scaledBitmap == null) {
                options.inSampleSize = 4;
                options.inJustDecodeBounds = false;
                scaledBitmap = BitmapFactory.decodeFile(path, options);
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);
            }
            scaledBitmap = changeRotation(scaledBitmap, path);
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);

            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return f;
        } catch (Exception e) {
            e.printStackTrace();
            return f;
        }
        if (strMyImagePath == null) {
            return f;
        }
        return savedFile;
    }

    public static File compressFile(String orignalFile) {
        File decodedFile = new File(orignalFile);
        long fileSizeInBytes = decodedFile.length();
        //Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
        long fileSizeInKB = fileSizeInBytes / 1024;
        if (fileSizeInKB > 700) {
            //    Log.i(TAG, "compressing Image");
            decodedFile = decodeFile(orignalFile);
            //     Log.i(TAG, "deleting old Image");
            deleteFiles(true, new File(orignalFile));
        }
        decodedFile = rotateAndSaveImage(decodedFile.getAbsolutePath(), BitmapFactory.decodeFile(decodedFile.getAbsolutePath()));
//        Log.v(TAG, "changing rotation of Image");
        return decodedFile;
    }

    public static boolean deleteFiles(boolean deleteFileAfterUpload, File decodedFile) {
        boolean isFileDeleted = false;
        if (deleteFileAfterUpload) {
            //      Log.i(TAG, "deleting file");
            if (decodedFile != null)
                isFileDeleted = decodedFile.delete();
            //    Log.i(TAG, "is file deleted: " + isFileDeleted);
        }
        return isFileDeleted;
    }

    public static void showProgressDialog(Context context, String msg, Boolean cancelable) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(cancelable);
        progressDialog.setMessage(msg);
        progressDialog.show();
    }

    public static int dpFromPx(final Context context, final float px) {
        return (int) (px / context.getResources().getDisplayMetrics().density);
    }

    public static int pxFromDp(final Context context, final float dp) {
        return (int) (dp * context.getResources().getDisplayMetrics().density);
    }

    public static SpannableStringBuilder getBoldText(Activity context, String text, int start, int end, int color) {
        text = text.trim();
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(text);
        spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableStringBuilder.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, color)), start, end, 0);
        return spannableStringBuilder;
    }

    public static String getFacebookKeyHash(Context context) {
        String keyhash = "";

        try {
            @SuppressLint("PackageManagerGetSignatures") PackageInfo info = context.getPackageManager().getPackageInfo(
                    context.getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                keyhash = Base64.encodeToString(md.digest(), Base64.DEFAULT);
                Log.d("KeyHash:", keyhash);
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException ignored) {

        }
        return keyhash;
    }

    public static boolean isValidEmail(String target) {
        return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static void setFontAwesome(View[] views) {
        Typeface openSans = Typeface.createFromAsset(views[0].getContext().getAssets(), "FontAwesome.otf");

        for (View view : views) {
            if (view instanceof TextView)
                ((TextView) view).setTypeface(openSans);
            if (view instanceof EditText)
                ((EditText) view).setTypeface(openSans);
            if (view instanceof Button)
                ((Button) view).setTypeface(openSans);
        }
    }


}
