package com.pantera.mobin.serv_u.helpers;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.pantera.mobin.serv_u.R;

/**
 * Created by Mobin on 5/15/2017.
 */

public class FragmentHelper {
    public static void restartFragment(AppCompatActivity context, Fragment fragment) {
        FragmentTransaction ft = context.getSupportFragmentManager().beginTransaction();
        ft.detach(fragment).attach(fragment).commit();
    }

    public static void replaceFragment(AppCompatActivity context, Fragment fragment, Boolean stateLoss) {
        FragmentTransaction ft = context.getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment, fragment);
        // ft.addToBackStack(fragment.getClass().getSimpleName());
        // to use back button above logic use popbackstack
        if (stateLoss)
            ft.commitAllowingStateLoss();
        else
            ft.commit();
    }

    public static void removeFragment(AppCompatActivity context, Fragment fragment) {
        FragmentTransaction ft = context.getSupportFragmentManager().beginTransaction();
        ft.remove(fragment);
        ft.commit();

    }

    public static void loadFragment(AppCompatActivity context, android.support.v4.app.Fragment fragment) {
        android.support.v4.app.FragmentManager fragmentManager = context.getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment, fragment).commit();
    }
}
