package com.pantera.mobin.serv_u.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.pantera.mobin.serv_u.R;
import com.pantera.mobin.serv_u.SERVU;
import com.pantera.mobin.serv_u.api.ApiSERVU;
import com.pantera.mobin.serv_u.interfaces.OnSignUpListener;
import com.pantera.mobin.serv_u.models.Auth;
import com.pantera.mobin.serv_u.models.SignUpInput;
import com.pantera.mobin.serv_u.models.UploadImage;
import com.pantera.mobin.serv_u.utils.CircleTranform;
import com.pantera.mobin.serv_u.utils.Constant;
import com.pantera.mobin.serv_u.utils.util;
import com.samkazmi.simpleimageselect.Config;
import com.samkazmi.simpleimageselect.SimpleImageSelect;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;

public class SignUpActivity extends BaseActivity implements OnSignUpListener {

    SwitchCompat switchCompat;
    EditText companyE, phnE, nameE, emailE, passwordE, confirmPasswordE, aboutE;
    ImageView addPhoto;
    String image;

    @Override
    protected void initViews() {
        switchCompat = (SwitchCompat) findViewById(R.id.spSwitch);
        companyE = (EditText) findViewById(R.id.company);
        passwordE = (EditText) findViewById(R.id.password);
        confirmPasswordE = (EditText) findViewById(R.id.confirmPass);
        nameE = (EditText) findViewById(R.id.name);
        phnE = (EditText) findViewById(R.id.phone);
        emailE = (EditText) findViewById(R.id.email);
        aboutE = (EditText) findViewById(R.id.about);
        addPhoto = (ImageView) findViewById(R.id.addphoto);
    }

    @Override
    protected void setListeners() {
        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    companyE.setVisibility(View.VISIBLE);
                } else {
                    companyE.setVisibility(View.GONE);
                }

            }
        });
    }


    public void addPhoto(View v) {

        SimpleImageSelect.chooseSingleImage(Config.TYPE_CHOOSER_BOTH, this);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            image = SimpleImageSelect.onActivityResult(this, requestCode, resultCode, data);
            if (image != null) {
                Picasso.with(this).load(new File(image)).transform(new CircleTranform()).into(addPhoto);
                File file = util.compressFile(image);
                String encoded = util.encodeImageToBase64(file.getAbsolutePath());
                util.showProgressDialog(this, "Uploading...", false);
                ApiSERVU.uploadPic(this, new UploadImage(encoded));

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void trySignUP(View view) {
        String name = nameE.getText().toString().trim();
        String email = emailE.getText().toString().trim();
        String phn = phnE.getText().toString().trim();
        String password = passwordE.getText().toString().trim();
        String confirm = confirmPasswordE.getText().toString().trim();
        String company = companyE.getText().toString().trim();
        String about = aboutE.getText().toString().trim();

        if (phn.isEmpty() && email.isEmpty()) {
            Toast.makeText(this, getString(R.string.one), Toast.LENGTH_SHORT).show();
        }

        if (name.isEmpty()) {
            nameE.setError(getString(R.string.hint_name));
        }
        if (!email.isEmpty() && !util.isValidEmail(email)) {
            //emailE.setError(getString(R.string.hint_email));

            emailE.setError(getString(R.string.invalid_email));
        }

        if (!phn.isEmpty() && phn.length() < 12) {
            // phnE.setError(getString(R.string.hint_phn));

            phnE.setError(getString(R.string.short_phn));
        }

        if (password.isEmpty()) {
            passwordE.setError(getString(R.string.hint_password));
        } else if (password.length() < 6) {
            passwordE.setError(getString(R.string.short_pass));
        } else if (confirm.isEmpty()) {
            confirmPasswordE.setError(getString(R.string.hint_cnfrm));
        } else if (!confirm.equals(password)) {
            confirmPasswordE.setError(getString(R.string.unmatch_password));
        }
        if (name.length() > 0 && (util.isValidEmail(email) || phn.length() >= 12) && !confirm.isEmpty() && confirm.equals(password) && password.length() >= 6) {
            util.showProgressDialog(this, "Signing up...", false);
            SignUpInput signUpInput = new SignUpInput();
            signUpInput.setName(name);
            if (util.isValidEmail(email)) {
                signUpInput.setEmail(email);
            }
            if (phn.length() >= 12) {
                signUpInput.setPhone(phn);
            }
            signUpInput.setPassword(password);
            signUpInput.setConfirmPassword(confirm);

            signUpInput.setAbout(about);

            if (company.length() > 0 && switchCompat.isChecked()) {
                signUpInput.setCompany_name(company);
            } else {
                signUpInput.setCompany_name(null);
            }
            if (Constant.profile == null) {
                signUpInput.setProfile_pic_id(null);
            } else {
                signUpInput.setProfile_pic_id(Constant.profile.getId());

            }
            ApiSERVU.doSignUp(this, signUpInput);
        }


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        initViews();
        showToolBar("Sign Up", true);
        setListeners();
        SimpleImageSelect.Config(this, "Choose Profile Picture from", getString(R.string.app_name));
    }


    @Override
    public void OnSignUPSuccess(Auth auth) {
        util.dismissProgressDialog();
        if (TextUtils.isEmpty(auth.getUser().getCompany_name())) {
            SERVU.saveLogin(auth);
            startActivity(new Intent(this, MainActivity.class));
            finish();
        } else {
            showMsgDialog("You have successfully applied for Service Provider till than you can avail services of a General User until approved.");
        }


    }

    @Override
    public void OnSignUPFailed(String error) {
        util.dismissProgressDialog();
        showMsgDialog(error);

    }
}
