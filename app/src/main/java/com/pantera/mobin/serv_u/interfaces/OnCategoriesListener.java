package com.pantera.mobin.serv_u.interfaces;

import com.pantera.mobin.serv_u.models.CategoryPage;

/**
 * Created by Mobin on 6/5/2017.
 */

public interface OnCategoriesListener {
    void OnCategoriesReceived(CategoryPage categoryPage);

    void OnCategoriesFailure(String error);
}
