package com.pantera.mobin.serv_u.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.paginate.Paginate;
import com.pantera.mobin.serv_u.R;
import com.pantera.mobin.serv_u.activities.JobDetailActivity;
import com.pantera.mobin.serv_u.activities.MainActivity;
import com.pantera.mobin.serv_u.adapters.JobsAdapter;
import com.pantera.mobin.serv_u.api.ApiSERVU;
import com.pantera.mobin.serv_u.interfaces.OnJobClickedListener;
import com.pantera.mobin.serv_u.interfaces.OnJobListListener;
import com.pantera.mobin.serv_u.models.Job;
import com.pantera.mobin.serv_u.models.JobsPage;
import com.pantera.mobin.serv_u.utils.Constant;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link JobsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class JobsFragment extends Fragment implements OnJobListListener, OnJobClickedListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    Paginate paginate;
    List<Job> jobsList = new ArrayList<>();
    JobsAdapter adapter;
    RecyclerView recyclerView;

    // TODO: Rename and change types of parameters


    public JobsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     */
    // TODO: Rename and change types and number of parameters
    public static JobsFragment newInstance() {

        return new JobsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_jobs, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        ((MainActivity) getContext()).jobsToolbar();
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new JobsAdapter(this, jobsList);
        recyclerView.setAdapter(adapter);
        paginate = Paginate.with(recyclerView, callbacks).setLoadingTriggerThreshold(1)
                .addLoadingListItem(true).build();

    }

    private void getJobs() {
        ApiSERVU.getJobs(this, getPageNo(), getTimeStamp());
    }

    boolean isLoading = false;
    boolean hasLoadedAllItems = false;
    int pageNO;
    String timeStamp = null;

    Paginate.Callbacks callbacks = new Paginate.Callbacks() {
        @Override
        public void onLoadMore() {
            isLoading = true;
            getJobs();
        }

        @Override
        public boolean isLoading() {
            return isLoading;
        }

        @Override
        public boolean hasLoadedAllItems() {
            return hasLoadedAllItems;
        }
    };

    private int getPageNo() {
        if (pageNO > 0)
            pageNO++;
        return pageNO;
    }

    private String getTimeStamp() {
        if (pageNO == 1)
            return null;
        else
            return timeStamp;
    }


    @Override
    public void OnJobsFetched(JobsPage jobs) {
        isLoading = false;
        pageNO = jobs.getPage();
        timeStamp = jobs.getTimeStamp();
        hasLoadedAllItems = !jobs.isMore_available();
        jobsList.addAll(jobs.getJobs());


        if (pageNO == 1) {
            if (jobsList.size() == 0) {
                ((MainActivity) getContext()).showErrorLayout("No Jobs Found");
                hasLoadedAllItems = true;
            }
            adapter.notifyItemRangeChanged(0, jobsList.size());
        } else {
            int lastIndex = jobsList.size() - 1;
            if (lastIndex != -1) {
                adapter.notifyItemRangeChanged(lastIndex, jobsList.size());
            }

        }

    }

    @Override
    public void OnJobsFetchFailed(String error) {
        isLoading = false;
        hasLoadedAllItems = true;
        paginate.setHasMoreDataToLoad(false);
        ((MainActivity) getContext()).showErrorLayout(error);

    }

    @Override
    public void OnJobClicked(Job job, int position) {
        //Toast.makeText(getContext(),position+"",Toast.LENGTH_SHORT).show();
        Intent intent = new Intent();
        intent.setClass(getContext(), JobDetailActivity.class);
        intent.putExtra(Constant.key_jobID, job.getId());
        startActivity(intent);


    }
}
