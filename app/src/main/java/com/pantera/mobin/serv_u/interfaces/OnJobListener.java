package com.pantera.mobin.serv_u.interfaces;

import com.pantera.mobin.serv_u.models.Job;

/**
 * Created by Mobin on 6/13/2017.
 */

public interface OnJobListener {

    void OnJobAdded(Job job);
    void OnJobFailed(String error);
}
