package com.pantera.mobin.serv_u.models;

/**
 * Created by Mobin on 5/24/2017.
 */

public class Comment {
    private int id,job_id;
    private String body,created_at;
    private User user;

    public int getId() {
        return id;
    }

    public int getJob_id() {
        return job_id;
    }

    public String getBody() {
        return body;
    }

    public String getCreated_at() {
        return created_at;
    }

    public User getUser() {
        return user;
    }
}
