package com.pantera.mobin.serv_u.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.paginate.Paginate;
import com.pantera.mobin.serv_u.R;
import com.pantera.mobin.serv_u.adapters.CategoryAdapterSP;
import com.pantera.mobin.serv_u.api.ApiSERVU;
import com.pantera.mobin.serv_u.interfaces.OnCategoriesListener;
import com.pantera.mobin.serv_u.models.Category;
import com.pantera.mobin.serv_u.models.CategoryPage;

import java.util.ArrayList;
import java.util.List;

public class CategoryActivity extends BaseActivity implements OnCategoriesListener {
    RecyclerView recyclerView;
    boolean isLoading = false;
    boolean hasLoadedAllItems = false;
    int pageNO;
    String timeStamp;
    Paginate paginate;
    CategoryAdapterSP adapter;
    List<Category> categoryList = new ArrayList<>();
    Paginate.Callbacks callbacks = new Paginate.Callbacks() {
        @Override
        public void onLoadMore() {
            isLoading = true;
            getCategories();
        }

        @Override
        public boolean isLoading() {
            return isLoading;
        }

        @Override
        public boolean hasLoadedAllItems() {
            return hasLoadedAllItems;
        }
    };

    private int getPageNo() {
        if (pageNO > 0)
            pageNO++;
        return pageNO;
    }

    private String getTimeStamp() {
        if (pageNO == 1)
            return null;
        else
            return timeStamp;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_category);
        initViews();
        setListeners();

    }

    @Override
    void initViews() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new CategoryAdapterSP(categoryList);
        recyclerView.setAdapter(adapter);
        paginate = Paginate.with(recyclerView, callbacks).setLoadingTriggerThreshold(1)
                .addLoadingListItem(true).build();
    }

    private void getCategories() {
        ApiSERVU.getCategories(this, getPageNo(), getTimeStamp());
    }

    @Override
    void setListeners() {

    }

    @Override
    public void OnCategoriesReceived(CategoryPage categoryPage) {
        isLoading = false;
        pageNO = categoryPage.getPage();
        timeStamp = categoryPage.getTimeStamp();
        hasLoadedAllItems = !categoryPage.isMore_available();
        categoryList.addAll(categoryPage.getCategories());

        if (pageNO == 1) {
            if (categoryList.size() == 0) {

                Toast.makeText(this, "No categories Found", Toast.LENGTH_SHORT).show();
            }
            adapter.notifyItemRangeChanged(0, categoryList.size());


        } else {
            int lastIndex = categoryList.size() - 1;
            if (lastIndex != -1) {
                adapter.notifyItemRangeChanged(lastIndex + 1, categoryList.size());
            } else {
                adapter.notifyItemRangeChanged(0, categoryList.size());
            }

        }
    }

    @Override
    public void OnCategoriesFailure(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
        isLoading = false;
        hasLoadedAllItems = true;
        paginate.setHasMoreDataToLoad(false);
    }
}
