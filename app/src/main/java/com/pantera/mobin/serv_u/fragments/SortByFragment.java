package com.pantera.mobin.serv_u.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pantera.mobin.serv_u.R;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class SortByFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


    // TODO: Rename and change types of parameters



    public SortByFragment() {
        // Required empty public constructor
    }
    // TODO: Rename and change types and number of parametersreturn new SortByFragment();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sort_by, container, false);
    }

}
