package com.pantera.mobin.serv_u.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pantera.mobin.serv_u.R;
import com.pantera.mobin.serv_u.models.Notification;
import com.pantera.mobin.serv_u.utils.util;

import java.util.List;

/**
 * Created by Mobin on 5/18/2017.
 */

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.MyViewHolder> {

    private List<Notification> list;

    public NotificationsAdapter(List<Notification> list) {
        this.list = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(View.inflate(parent.getContext(), R.layout.profile_adapter, null));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Notification notification = list.get(position);

        holder.time.setText(util.timeFormat(notification.getCreated_at()));
        holder.messageType.setText(notification.getMessage());
        holder.name.setText(notification.getCreator().getName());

        // holder.job.setText(notification.getCreator().);


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name, job, messageType, time;
        ImageView userPic;


        MyViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            job = (TextView) itemView.findViewById(R.id.job);
            messageType = (TextView) itemView.findViewById(R.id.comment);
            time = (TextView) itemView.findViewById(R.id.time);
            userPic = (ImageView) itemView.findViewById(R.id.avatar);
            job.setVisibility(View.GONE);


        }
    }
}
