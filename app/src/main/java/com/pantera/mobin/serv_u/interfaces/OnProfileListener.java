package com.pantera.mobin.serv_u.interfaces;

import com.pantera.mobin.serv_u.models.User;

/**
 * Created by Mobin on 5/18/2017.
 */

public interface OnProfileListener {
    void OnUserProfileReceived(User user);

    void OnUserProfileFailure(String error);
}
