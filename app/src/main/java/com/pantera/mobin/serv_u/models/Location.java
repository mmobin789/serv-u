package com.pantera.mobin.serv_u.models;

/**
 * Created by Mobin on 5/16/2017.
 */

public class Location {

    public int getId() {
        return id;
    }

    public int getUser_id() {
        return user_id;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public String getStreet() {
        return street;
    }

    public String getArea() {
        return area;
    }

    public String getPhone() {
        return phone;
    }

    public String getCreated_at() {
        return created_at;
    }

    public Country getCountry() {
        return country;
    }

    public City getCity() {
        return city;
    }

    int id,user_id;
    Double latitude,longitude;
    String street,area,phone,created_at;
    Country country;
    City city;

}
