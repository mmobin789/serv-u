package com.pantera.mobin.serv_u.helpers;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

/**
 * Created by Mobin on 5/29/2017.
 */

public class MobiSeekBar extends android.support.v7.widget.AppCompatSeekBar {
    Drawable thumb;

    public MobiSeekBar(Context context) {
        super(context);
    }

    public MobiSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setThumb(Drawable thumb) {
        super.setThumb(thumb);
        this.thumb = thumb;
    }

    public Drawable getThumb()
    {
        return thumb;
    }
}
