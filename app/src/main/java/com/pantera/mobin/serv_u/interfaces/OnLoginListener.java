package com.pantera.mobin.serv_u.interfaces;

import com.pantera.mobin.serv_u.models.Auth;

/**
 * Created by Mobin on 5/10/2017.
 */

public interface OnLoginListener {

    void onLoginSuccess(Auth user);
    void onLoginFailed(String error);
}
