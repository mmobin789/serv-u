package com.pantera.mobin.serv_u.activities;

import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pantera.mobin.serv_u.R;
import com.pantera.mobin.serv_u.adapters.FilterPagerAdapter;
import com.pantera.mobin.serv_u.helpers.MobiViewPager;

public class FilterActivitySP extends BaseActivity implements View.OnClickListener {
   public ViewPager viewPager;
    //   TextView sortBy, category, budget, distance;
    public TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_sp);
        initViews();
        setListeners();

    }


    @Override
    void initViews() {
        setToolBar();

//        sortBy = (TextView) findViewById(R.id.sortBy);
//        category = (TextView) findViewById(R.id.cat);
//        budget = (TextView) findViewById(R.id.budget);
//        distance = (TextView) findViewById(R.id.dis);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (MobiViewPager) findViewById(R.id.filterPager);
        viewPager.setAdapter(new FilterPagerAdapter(this, getSupportFragmentManager(), 4));
        viewPager.setOffscreenPageLimit(4);
        tabLayout.setupWithViewPager(viewPager);
        setTabBorderUI();
        //   tabLayout.setSelectedTabIndicatorColor(Color.TRANSPARENT);
        //  tabLayout.setTabTextColors(ContextCompat.getColor(this,R.color.cardText),ContextCompat.getColor(this,R.color.btn_border));
        //  tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        // tabLayout.setupWithViewPager(viewPager);

        //  customTabLayout();


    }

    private void setTabBorderUI() {
        tabLayout.setBackgroundResource(R.drawable.border);
        LinearLayout l = (LinearLayout) tabLayout.getChildAt(0);
        l.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
        GradientDrawable g = new GradientDrawable();
        g.setColor(ContextCompat.getColor(this, R.color.settings_header));
        g.setSize(getResources().getDimensionPixelOffset(R.dimen._1sdp), getResources().getDimensionPixelOffset(R.dimen._1sdp));
        l.setDividerDrawable(g);

    }


    private void setToolBar() {
        showToolBar(getString(R.string.filter), false);
        TextView cancel = (TextView) findViewById(R.id.cancel);
        TextView done = (TextView) findViewById(R.id.editTxt);
        cancel.setVisibility(View.VISIBLE);
        done.setVisibility(View.VISIBLE);
        done.setText(getString(R.string.done));
        done.setOnClickListener(this);
        cancel.setOnClickListener(this);

    }

    @Override
    void setListeners() {
        //  viewPager.addOnPageChangeListener(this);
//        sortBy.setOnClickListener(this);
//        distance.setOnClickListener(this);
//        budget.setOnClickListener(this);
//        category.setOnClickListener(this);
//        sortBy.performClick();


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.editTxt:
                break;
            case R.id.cancel:
                onBackPressed();
                break;
        }
    }


//    private void sortBySelected() {
//        int black = ContextCompat.getColor(this, R.color.cardText);
//        int blue = ContextCompat.getColor(this, R.color.btn_border);
//        sortBy.setTextColor(blue);
//        distance.setTextColor(black);
//        category.setTextColor(black);
//        budget.setTextColor(black);
//    }
//
//    private void budgetSelected() {
//        int black = ContextCompat.getColor(this, R.color.cardText);
//        int blue = ContextCompat.getColor(this, R.color.btn_border);
//        sortBy.setTextColor(black);
//         distance.setTextColor(black);
//        category.setTextColor(black);
//       budget.setTextColor(blue);
//    }

//    private void distanceSelected() {
//        int black = ContextCompat.getColor(this, R.color.cardText);
//        int blue = ContextCompat.getColor(this, R.color.btn_border);
//        sortBy.setTextColor(black);
//        distance.setTextColor(blue);
//        category.setTextColor(black);
//        budget.setTextColor(black);
//
//    }

//    private void categorySelected() {
//        int black = ContextCompat.getColor(this, R.color.cardText);
//        int blue = ContextCompat.getColor(this, R.color.btn_border);
//        sortBy.setTextColor(black);
//        distance.setTextColor(black);
//        category.setTextColor(blue);
//        budget.setTextColor(black);
//    }


}

