package com.pantera.mobin.serv_u.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.paginate.Paginate;
import com.pantera.mobin.serv_u.R;
import com.pantera.mobin.serv_u.activities.JobDetailActivity;
import com.pantera.mobin.serv_u.adapters.CommentsAdapter;
import com.pantera.mobin.serv_u.api.ApiSERVU;
import com.pantera.mobin.serv_u.interfaces.OnCommentsListener;
import com.pantera.mobin.serv_u.models.Comment;
import com.pantera.mobin.serv_u.models.CommentsPage;
import com.pantera.mobin.serv_u.utils.Constant;

import java.util.ArrayList;
import java.util.List;


public class CommentsFragment extends Fragment implements OnCommentsListener {
    /// TODO: Rename parameter arguments, choose names that match

    int jobID;
    RecyclerView recyclerView;
    boolean isLoading = false;
    boolean hasLoadedAllItems = false;
    int pageNO;
    String timeStamp;
    List<Comment> commentsList = new ArrayList<>();
    CommentsAdapter adapter;
    Paginate paginate;

    // TODO: Rename and change types of parameters
    Paginate.Callbacks callbacks = new Paginate.Callbacks() {
        @Override
        public void onLoadMore() {
            isLoading = true;
            getComments();


        }

        @Override
        public boolean isLoading() {
            return isLoading;
        }

        @Override
        public boolean hasLoadedAllItems() {
            return hasLoadedAllItems;
        }
    };

    private void getComments() {
        ApiSERVU.getJobComments(this, jobID, getPageNo(), getTimeStamp());
    }

    public CommentsFragment() {
        // Required empty public constructor
    }

    private int getPageNo() {
        if (pageNO > 0)
            pageNO++;
        return pageNO;
    }

    private String getTimeStamp() {
        if (pageNO == 1)
            return null;
        else
            return timeStamp;
    }

    // TODO: Rename and change types and number of parameters
    public static CommentsFragment newInstance(int jobID) {
        CommentsFragment fragment = new CommentsFragment();
        Bundle args = new Bundle();
        args.putInt(Constant.key_jobID, jobID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new CommentsAdapter(commentsList);
        recyclerView.setAdapter(adapter);
        paginate = Paginate.with(recyclerView, callbacks).setLoadingTriggerThreshold(1)
                .addLoadingListItem(true).build();


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            jobID = getArguments().getInt(Constant.key_jobID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_comments, container, false);
    }

    @Override
    public void OnCommentsFetched(CommentsPage comments) {
        isLoading = false;
        pageNO = comments.getPage();
        timeStamp = comments.getTimeStamp();
        hasLoadedAllItems = !comments.isMore_available();
        commentsList.addAll(comments.getComments());


        if (pageNO == 1) {
            if (commentsList.size() == 0) {
                ((JobDetailActivity) getContext()).showErrorLayout(getView(),"No Comments Found");

            }
            adapter.notifyItemRangeChanged(0, commentsList.size());


        } else {
            int lastIndex = commentsList.size() - 1;
            if (lastIndex != -1) {
                adapter.notifyItemRangeChanged(lastIndex + 1, commentsList.size());
            } else {
                adapter.notifyItemRangeChanged(0, commentsList.size());
            }

        }

    }

    @Override
    public void OnCommentsFailure(String error) {
        ((JobDetailActivity) getContext()).showErrorLayout(getView(),error);
        Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
        isLoading = false;
        hasLoadedAllItems = true;
        paginate.setHasMoreDataToLoad(false);
    }
}
