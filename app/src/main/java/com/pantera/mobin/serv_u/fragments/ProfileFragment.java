package com.pantera.mobin.serv_u.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.pantera.mobin.serv_u.R;
import com.pantera.mobin.serv_u.activities.MainActivity;
import com.pantera.mobin.serv_u.api.ApiSERVU;
import com.pantera.mobin.serv_u.interfaces.OnProfileListener;
import com.pantera.mobin.serv_u.models.User;
import com.pantera.mobin.serv_u.utils.CircleTranform;
import com.pantera.mobin.serv_u.utils.util;
import com.squareup.picasso.Picasso;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment implements OnProfileListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    TextView name, reviews, skills, loc, jobCount;
    RatingBar ratingBar;
    ImageView avatar;

    // TODO: Rename and change types of parameters

    private void setProfileView(User user) {

        name.setText(user.getName());
        ratingBar.setRating(user.getAverage_rating());
        reviews.setText(user.getTotal_review_count() + "\n" + "reviews");
        skills.setText(user.getTotal_skill_count() + "\n" + "skills");
        jobCount.setText(user.getTotal_jobs_completed() + "\n" + "Jobs");
        if (user.getProfile_pic() != null)
            Picasso.with(getActivity()).load(user.getProfile_pic().getUrl()).transform(new CircleTranform()).into(avatar);
        else avatar.setImageResource(R.drawable.round);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        ((MainActivity) getContext()).profileToolBar();
        avatar = (ImageView) view.findViewById(R.id.avatar);
        name = (TextView) view.findViewById(R.id.name);
        ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
        reviews = (TextView) view.findViewById(R.id.reviews);
        skills = (TextView) view.findViewById(R.id.skills);
        loc = (TextView) view.findViewById(R.id.loc);
        jobCount = (TextView) view.findViewById(R.id.jobs);
        util.showProgressDialog(getContext(), "Loading Profile...", false);
        ApiSERVU.getProfile(this);


    }


    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance() {
        //  Bundle args = new Bundle();
        // args.putString(ARG_PARAM1, param1);
        // args.putString(ARG_PARAM2, param2);
        // fragment.setArguments(args);
        return new ProfileFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void OnUserProfileReceived(User user) {
        setProfileView(user);
        util.dismissProgressDialog();

    }

    @Override
    public void OnUserProfileFailure(String error) {

        ((MainActivity) getContext()).showErrorLayout(error);
        util.dismissProgressDialog();
    }
}
