package com.pantera.mobin.serv_u.fragments;


import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.pantera.mobin.serv_u.R;
import com.pantera.mobin.serv_u.helpers.MobiSeekBar;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class DistanceFragment extends Fragment implements SeekBar.OnSeekBarChangeListener {
    // TODO: Rename parameter arguments, choose names that match
    MobiSeekBar seekBar;

    TextView floatingProgress;
    // TODO: Rename and change types of parameters


    public DistanceFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    //    setRetainInstance(true);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        seekBar = (MobiSeekBar) view.findViewById(R.id.seekBar);
        floatingProgress = (TextView) view.findViewById(R.id.status);
        seekBar.setOnSeekBarChangeListener(this);

        //   seekBar.getProgressDrawable().setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_IN);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_distance, container, false);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(getResources().getDimensionPixelOffset(R.dimen._90sdp), RelativeLayout.LayoutParams.WRAP_CONTENT);
        // params.addRule(RelativeLayout.ABOVE,seekBar.getId());
        Rect thumbRect = seekBar.getThumb().getBounds();
        params.setMargins(thumbRect.centerX(), getContext().getResources().getDimensionPixelOffset(R.dimen._24sdp), 0, 0);
        floatingProgress.setLayoutParams(params);
        floatingProgress.setText("" + progress + "km");


    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        floatingProgress.setVisibility(View.VISIBLE);

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        floatingProgress.setVisibility(View.VISIBLE);
    }
}
