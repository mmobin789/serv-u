package com.pantera.mobin.serv_u.models;

/**
 * Created by Mobin on 5/17/2017.
 */

public class UploadImage {
    private String success;
    private String input;

    public UploadImage(String image) {
        this.input = "data:image/jpg;base64,"+image;
    }

    public String getSuccess() {
        return success;
    }
}
