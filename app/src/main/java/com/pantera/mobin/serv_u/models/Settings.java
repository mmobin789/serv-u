package com.pantera.mobin.serv_u.models;

/**
 * Created by Mobin on 5/12/2017.
 */

public class Settings {
    private String content;
    private Boolean checked;
    private int viewType;

    public Settings(String header, int viewType) {
        this.content = header;
        this.viewType = viewType;
    }

    public Settings(String content, Boolean checked, int viewType) {
        this.content = content;
        this.checked = checked;
        this.viewType = viewType;
    }


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }
}
