package com.pantera.mobin.serv_u.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.azoft.carousellayoutmanager.CarouselLayoutManager;
import com.azoft.carousellayoutmanager.CarouselZoomPostLayoutListener;
import com.azoft.carousellayoutmanager.CenterScrollListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.pantera.mobin.serv_u.R;
import com.pantera.mobin.serv_u.activities.JobDetailActivity;
import com.pantera.mobin.serv_u.adapters.CarouselAdapter;
import com.pantera.mobin.serv_u.api.ApiSERVU;
import com.pantera.mobin.serv_u.interfaces.OnJobDetailListener;
import com.pantera.mobin.serv_u.models.Job;
import com.pantera.mobin.serv_u.utils.Constant;
import com.pantera.mobin.serv_u.utils.util;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link JobDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class JobDetailsFragment extends Fragment implements OnJobDetailListener, View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match


    TextView startedOn, completedOn, startCode, scheduledFor, location, info, des;
    LinearLayout startedCode, started, completed, scheduled, jobDates;
    Button viewBids, fileComplaint, gotoChat;
    View lineCom, lineStart, lineCode, lineScheduled;
    RecyclerView carouselView;

    // TODO: Rename and change types of parameters
    private int jobID;


    private void populateView(Job job) {

        ((JobDetailActivity) getContext()).setJobStatus(job.getStatus());
        ((JobDetailActivity) getContext()).setJobHeadline(job.getBudget() + "", util.dateFormat(job.getCreated_at()));
        if (job.getStatus() == 1) // open
        {
            jobDates.setVisibility(View.GONE);
            gotoChat.setVisibility(View.GONE);
            fileComplaint.setVisibility(View.GONE);
            lineCom.setVisibility(View.GONE);
            lineStart.setVisibility(View.GONE);
            lineScheduled.setVisibility(View.GONE);
            lineCode.setVisibility(View.GONE);
        }
        if (job.getStatus() == 2) //awarded
        {

            viewBids.setVisibility(View.GONE);
            fileComplaint.setVisibility(View.GONE);
            started.setVisibility(View.GONE);
            completed.setVisibility(View.GONE);
            lineStart.setVisibility(View.GONE);
            lineCom.setVisibility(View.GONE);


        }
//        if (job.getStatus() == 3) //ongoing
//        {
//            // to do here
//        }

        if (job.getStatus() == 4) //completed
        {
            startedCode.setVisibility(View.GONE);
            scheduled.setVisibility(View.GONE);
            viewBids.setText(getString(R.string.review));
            gotoChat.setVisibility(View.GONE);
            started.setVisibility(View.GONE);
            lineStart.setVisibility(View.GONE);
            lineCode.setVisibility(View.GONE);
            lineScheduled.setVisibility(View.GONE);
        }

        startCode.setText(job.getStart_code());
        scheduledFor.setText(job.getSchedule());
        location.setText(job.getLocation().getArea());
        //  completedOn.setText(job.);
        info.setText(job.getAdditional_info());
        des.setText(job.getDescription());
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            list.add(R.drawable.pip);
        }
        carouselView.setAdapter(new CarouselAdapter(list));
        carouselView.scrollToPosition(list.size() / 2);
        setupMap(job.getLocation().getLatitude(), job.getLocation().getLongitude());

    }

    private void setupMap(final Double latitude, final Double longitude) {

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                MarkerOptions marker = new MarkerOptions();
                marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.location_red));
                LatLng l = new LatLng(latitude, longitude);
                marker.position(l);
                Marker m = googleMap.addMarker(marker);
                animateCameraForMap(l, googleMap);


            }
        });
    }

    private void animateCameraForMap(LatLng latLng, GoogleMap mMap) {
        CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(13).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        // Stop scrolling of map
        mMap.getUiSettings().setScrollGesturesEnabled(false);
        mMap.getUiSettings().setMapToolbarEnabled(false);
    }

    public JobDetailsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * .
     *
     * @return A new instance of fragment JobDetailsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static JobDetailsFragment newInstance(int jobID) {
        JobDetailsFragment fragment = new JobDetailsFragment();
        Bundle args = new Bundle();
        args.putInt(Constant.key_jobID, jobID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            jobID = getArguments().getInt(Constant.key_jobID);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_job_details, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        startCode = (TextView) view.findViewById(R.id.startcode);
        scheduledFor = (TextView) view.findViewById(R.id.scheduledFor);
        completedOn = (TextView) view.findViewById(R.id.completedOn);
        startedOn = (TextView) view.findViewById(R.id.startedOn);
        location = (TextView) view.findViewById(R.id.loc);
        startedCode = (LinearLayout) view.findViewById(R.id.startedCode);
        completed = (LinearLayout) view.findViewById(R.id.completed);
        info = (TextView) view.findViewById(R.id.info);
        carouselView = (RecyclerView) view.findViewById(R.id.recyclerView);
        des = (TextView) view.findViewById(R.id.des);
        scheduled = (LinearLayout) view.findViewById(R.id.scheduled);
        started = (LinearLayout) view.findViewById(R.id.started);
        viewBids = (Button) view.findViewById(R.id.viewBids);
        gotoChat = (Button) view.findViewById(R.id.gotoChat);
        fileComplaint = (Button) view.findViewById(R.id.fileComplaint);
        jobDates = (LinearLayout) view.findViewById(R.id.jobDates);
        lineCode = view.findViewById(R.id.lineCode);
        lineCom = view.findViewById(R.id.lineComp);
        lineStart = view.findViewById(R.id.lineStarted);
        lineScheduled = view.findViewById(R.id.lineSchedule);
        CarouselLayoutManager carouselLayoutManager = new CarouselLayoutManager(CarouselLayoutManager.HORIZONTAL, false);
        carouselLayoutManager.setPostLayoutListener(new CarouselZoomPostLayoutListener());
        carouselView.setHasFixedSize(true);
        carouselView.addOnScrollListener(new CenterScrollListener());
        carouselView.setLayoutManager(carouselLayoutManager);

        setListeners();
        getJobDetail();


    }

    private void setListeners() {
        viewBids.setOnClickListener(this);
        gotoChat.setOnClickListener(this);
        fileComplaint.setOnClickListener(this);
    }

    private void getJobDetail() {
       // util.showProgressDialog(getContext(), "Loading Job Details...", false);
        ApiSERVU.getJobDetail(this, jobID + "");
    }

    @Override
    public void OnJobDetailReceived(Job job) {
      //  util.dismissProgressDialog();
        populateView(job);


    }


    @Override
    public void OnJobDetailFailure(String error) {
        Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
        //util.dismissProgressDialog();
    }

    @Override
    public void onClick(View v) {

    }


}
