package com.pantera.mobin.serv_u.interfaces;

import com.pantera.mobin.serv_u.models.Auth;

/**
 * Created by Mobin on 5/15/2017.
 */

public interface OnSignUpListener {

    void OnSignUPSuccess(Auth auth);
    void OnSignUPFailed(String error);
}
