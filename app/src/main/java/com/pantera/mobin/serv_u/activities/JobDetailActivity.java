package com.pantera.mobin.serv_u.activities;

import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pantera.mobin.serv_u.R;
import com.pantera.mobin.serv_u.adapters.JobInfoAdapter;
import com.pantera.mobin.serv_u.utils.Constant;
import com.pantera.mobin.serv_u.utils.util;

public class JobDetailActivity extends BaseActivity implements View.OnClickListener {
    // TextView detail, bids, comments;
    private int jobID;
    ViewPager viewPager;
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_detail);
        initViews();
        showToolBar("Job Detail", true);
        setListeners();
        //    loadJobDetailsFragment();


    }

    public void showErrorLayout(View view, String error) {
        if (!util.isInternetAvailable(this) || TextUtils.isEmpty(error)) {
            error = "No Internet";
        }
        TextView textView = (TextView) view.findViewById(R.id.error);
        textView.setVisibility(View.VISIBLE);
        textView.setText(error);


    }

    public void setJobStatus(int status) {
        ImageView imageView = (ImageView) findViewById(R.id.status);
        int res = -1;
        if (status == 1)
            res = R.drawable.open_timeline;
        if (status == 2)
            res = R.drawable.awarded_timeline;
        if (status == 3)
            res = R.drawable.ongoing_timeline;
        if (status == 4)
            res = R.drawable.completed_timeline;

        imageView.setImageResource(res);

    }

    private void disableFragment(View v) {
        TextView textView = (TextView) v;
        textView.setTextColor(ContextCompat.getColor(this, R.color.btn_border));
        textView.setEnabled(false);

    }

    private void enableFragment(View v) {
        TextView textView = (TextView) v;
        textView.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        textView.setEnabled(true);

    }

    public void setJobHeadline(String budget, String createdAt) {
        TextView price = (TextView) findViewById(R.id.budget);
        TextView date = (TextView) findViewById(R.id.date);
        price.setText(budget);
        date.setText(createdAt);
    }

//    private void loadJobDetailsFragment() {
//        if (Constant.userType == 2) //sp
//        {
//            //      bids.setVisibility(View.GONE);
//            tabLayout.removeTabAt(1);
//        }
////
////        if (getIntent() != null) {
////            jobID = getIntent().getIntExtra(Constant.key_jobID, -1);
////        }
////        FragmentHelper.loadFragment(this, JobDetailsFragment.newInstance(jobID));
//    }

    @Override
    void initViews() {
        //   detail = (TextView) findViewById(R.id.details);
        //  bids = (TextView) findViewById(R.id.bids);
        //   comments = (TextView) findViewById(R.id.comments);
        tabLayout = (TabLayout) findViewById(R.id.jobTabs);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPager.setOffscreenPageLimit(3);
        if (getIntent() != null) {
            jobID = getIntent().getIntExtra(Constant.key_jobID, -1);
        }
        viewPager.setAdapter(new JobInfoAdapter(this, getSupportFragmentManager(), 3, jobID));
        tabLayout.setupWithViewPager(viewPager);
        setTabBorderUI();
        if (Constant.userType == 2) //sp
        {
            tabLayout.removeTabAt(1);
        }
        //  disableFragment(detail);
    }

    private void setTabBorderUI() {
        tabLayout.setBackgroundResource(R.drawable.border);
        LinearLayout l = (LinearLayout) tabLayout.getChildAt(0);
        l.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
        GradientDrawable g = new GradientDrawable();
        g.setColor(ContextCompat.getColor(this, R.color.settings_header));
        g.setSize(getResources().getDimensionPixelOffset(R.dimen._1sdp), getResources().getDimensionPixelOffset(R.dimen._1sdp));
        l.setDividerDrawable(g);

    }

    @Override
    void setListeners() {
//        detail.setOnClickListener(this);
//        bids.setOnClickListener(this);
//        comments.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
//        Fragment fragment;
//        disableFragment(v);
//        if (v.getId() == detail.getId()) {
//
//            enableFragment(comments);
//            enableFragment(bids);
//            fragment = JobDetailsFragment.newInstance(jobID);
//        } else if (v.getId() == comments.getId()) {
//            enableFragment(detail);
//            enableFragment(bids);
//            fragment = CommentsFragment.newInstance(jobID);
//
//
//        } else {
//            enableFragment(detail);
//            enableFragment(comments);
//            fragment = BidsFragment.newInstance(jobID);
//
//        }
//
//        FragmentHelper.loadFragment(this, fragment);
//
//    }
    }
}
