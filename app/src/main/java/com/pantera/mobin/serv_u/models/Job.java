package com.pantera.mobin.serv_u.models;

import java.util.List;

/**
 * Created by Mobin on 5/16/2017.
 */

public class Job {
    private String title, description, schedule, additional_info, start_code, created_at, awarded_on, started_at;
    private int id;
    private int job_type;

    public int getStatus() {
        return status;
    }

    private int status;
    private Double budget;
    private Duration duration;

    public Category getCategory() {
        return category;
    }

    public Service getService() {
        return service;
    }

    public Location getLocation() {
        return location;
    }

    public User getUser() {
        return user;
    }

    public Bid getAccepted_bid() {
        return accepted_bid;
    }

    public List<Document> getImages() {
        return images;
    }

    public String getTitle() {

        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getSchedule() {
        return schedule;
    }

    public String getAdditional_info() {
        return additional_info;
    }

    public String getStart_code() {
        return start_code;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getAwarded_on() {
        return awarded_on;
    }

    public String getStarted_at() {
        return started_at;
    }

    public int getId() {
        return id;
    }

    public int getJob_type() {
        return job_type;
    }

    public Double getBudget() {
        return budget;
    }

    public Duration getDuration() {
        return duration;
    }

    private Category category;
    private Service service;
    private Location location;
    private User user;
    private Bid accepted_bid;
    private List<Document> images;


}
