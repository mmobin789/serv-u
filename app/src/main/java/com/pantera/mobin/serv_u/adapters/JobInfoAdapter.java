package com.pantera.mobin.serv_u.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.pantera.mobin.serv_u.R;
import com.pantera.mobin.serv_u.fragments.BidsFragment;
import com.pantera.mobin.serv_u.fragments.CommentsFragment;
import com.pantera.mobin.serv_u.fragments.JobDetailsFragment;

/**
 * Created by Mobin on 6/2/2017.
 */

public class JobInfoAdapter extends FragmentStatePagerAdapter {
    private int tabCount, jobID;
    private String[] title;

    public JobInfoAdapter(Context context, FragmentManager fm, int tabCount, int jobID) {
        super(fm);
        this.tabCount = tabCount;
        this.jobID = jobID;
        title = new String[]{context.getString(R.string.details), context.getString(R.string.bids), context.getString(R.string.comments)};
    }

    @Override
    public Fragment getItem(int i) {
        Fragment fragment;
        switch (i) {
            case 0:
                fragment = JobDetailsFragment.newInstance(jobID);
                break;
            case 1:
                fragment = BidsFragment.newInstance(jobID);
                break;
            case 2:
                fragment = CommentsFragment.newInstance(jobID);
                break;
            default:
                fragment = JobDetailsFragment.newInstance(jobID);

        }

        return fragment;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
