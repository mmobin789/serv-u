package com.pantera.mobin.serv_u.models;

/**
 * Created by Mobin on 5/18/2017.
 */

public class Notification {

    public int getId() {
        return id;
    }

    public int getResource_id() {
        return resource_id;
    }

    public int getNotification_type() {
        return notification_type;
    }

    public int getUser_id() {
        return user_id;
    }

    public User getCreator() {
        return creator;
    }

    public boolean is_read() {
        return is_read;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getMessage() {
        return message;
    }

    private int id,resource_id,notification_type,user_id;
    private User creator;
    private boolean is_read;
    private String created_at,message;

}
