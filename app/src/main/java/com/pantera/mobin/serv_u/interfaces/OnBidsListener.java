package com.pantera.mobin.serv_u.interfaces;

import com.pantera.mobin.serv_u.models.BidsPage;

/**
 * Created by Mobin on 5/22/2017.
 */

public interface OnBidsListener {


    void OnBidsReceived(BidsPage bidsPage);
    void OnBidsFetchFailed(String error);
}
