package com.pantera.mobin.serv_u.models;

/**
 * Created by Mobin on 5/16/2017.
 */

public class Duration {
    public int getId() {
        return id;
    }

    public int getWork_hours() {
        return work_hours;
    }

    public int getWork_hours_per() {
        return work_hours_per;
    }

    public int getWage_type() {
        return wage_type;
    }

    public int getDuration_days() {
        return duration_days;
    }

    private int id,work_hours,work_hours_per,wage_type,duration_days;
}
