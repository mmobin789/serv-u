package com.pantera.mobin.serv_u.adapters;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.pantera.mobin.serv_u.R;
import com.pantera.mobin.serv_u.models.Settings;

import java.util.ArrayList;

/**
 * Created by Mobin on 5/12/2017.
 */

public class SettingsAdapter extends RecyclerView.Adapter<SettingsAdapter.ViewHolder> {
    private ArrayList<Settings> list;


    public SettingsAdapter(ArrayList<Settings> arrayList) {
        list = arrayList;
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position).getViewType();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0)
            return new Header(View.inflate(parent.getContext(), R.layout.settings_adapter_header, null));
        else
            return new SwitchHolder(View.inflate(parent.getContext(), R.layout.settings_adapter_layout, null));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Settings settings = list.get(position);
        switch (getItemViewType(position)) {
            case 0:
                Header header = (Header) holder;
                header.header.setText(settings.getContent());
                break;
            case 1:
                SwitchHolder switchHolder = (SwitchHolder) holder;
                switchHolder.content.setText(settings.getContent());
                switchHolder.aSwitch.setChecked(settings.getChecked());
                switchHolder.aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                        if (isChecked)
                            Toast.makeText(buttonView.getContext(), settings.getContent(), Toast.LENGTH_SHORT).show();
                        else {
                            Toast.makeText(buttonView.getContext(), settings.getContent() + " off", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                break;
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    private class SwitchHolder extends ViewHolder {
        TextView content;
        SwitchCompat aSwitch;

        SwitchHolder(View itemView) {
            super(itemView);
            content = (TextView) itemView.findViewById(R.id.content);
            aSwitch = (SwitchCompat) itemView.findViewById(R.id.switchSettings);
        }

    }

    private class Header extends ViewHolder {
        TextView header;

        Header(View itemView) {
            super(itemView);
            header = (TextView) itemView.findViewById(R.id.header);
        }

    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
