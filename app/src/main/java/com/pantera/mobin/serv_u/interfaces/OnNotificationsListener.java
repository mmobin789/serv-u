package com.pantera.mobin.serv_u.interfaces;

import com.pantera.mobin.serv_u.models.Notifications;

/**
 * Created by Mobin on 5/18/2017.
 */

public interface OnNotificationsListener {

    void OnNotificationListReceived(Notifications notifications);
    void OnNotificationsListFailure(String error);
}
