package com.pantera.mobin.serv_u.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pantera.mobin.serv_u.R;

/**
 * Created by Mobin on 5/19/2017.
 */

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.MessageViewHolder> {


    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MessageViewHolder(View.inflate(parent.getContext(), R.layout.profile_adapter, null));
    }

    @Override
    public void onBindViewHolder(MessageViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    class MessageViewHolder extends RecyclerView.ViewHolder {
        TextView comment, newMessageCount;

        public MessageViewHolder(View itemView) {
            super(itemView);
            comment = (TextView) itemView.findViewById(R.id.comment);
            comment.setVisibility(View.GONE);
            newMessageCount = (TextView) itemView.findViewById(R.id.newMessages);
        }
    }
}
