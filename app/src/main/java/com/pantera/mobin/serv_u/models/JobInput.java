package com.pantera.mobin.serv_u.models;

import java.util.List;

/**
 * Created by Mobin on 6/13/2017.
 */

public class JobInput {
    private String title,description,schedule,additional_info;
    private int category_id,service_id,budget,job_type,location_id;
    private Duration duration;
    private List<Integer> image_ids;

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public void setAdditional_info(String additional_info) {
        this.additional_info = additional_info;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public void setService_id(int service_id) {
        this.service_id = service_id;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    public void setJob_type(int job_type) {
        this.job_type = job_type;
    }

    public void setLocation_id(int location_id) {
        this.location_id = location_id;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public void setImage_ids(List<Integer> image_ids) {
        this.image_ids = image_ids;
    }
}
