package com.pantera.mobin.serv_u.interfaces;

import com.pantera.mobin.serv_u.models.JobsPage;

/**
 * Created by Mobin on 5/16/2017.
 */

public interface OnJobListListener {

    void OnJobsFetched(JobsPage Jobs);
    void OnJobsFetchFailed(String error);
}
