package com.pantera.mobin.serv_u.api;

import android.content.Context;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pantera.mobin.serv_u.interfaces.OnBidsListener;
import com.pantera.mobin.serv_u.interfaces.OnCategoriesListener;
import com.pantera.mobin.serv_u.interfaces.OnCommentsListener;
import com.pantera.mobin.serv_u.interfaces.OnJobDetailListener;
import com.pantera.mobin.serv_u.interfaces.OnJobListListener;
import com.pantera.mobin.serv_u.interfaces.OnJobListener;
import com.pantera.mobin.serv_u.interfaces.OnLogOutListener;
import com.pantera.mobin.serv_u.interfaces.OnLoginListener;
import com.pantera.mobin.serv_u.interfaces.OnNotificationsListener;
import com.pantera.mobin.serv_u.interfaces.OnProfileListener;
import com.pantera.mobin.serv_u.interfaces.OnSignUpListener;
import com.pantera.mobin.serv_u.models.Auth;
import com.pantera.mobin.serv_u.models.BidsPage;
import com.pantera.mobin.serv_u.models.CategoryPage;
import com.pantera.mobin.serv_u.models.CommentsPage;
import com.pantera.mobin.serv_u.models.Document;
import com.pantera.mobin.serv_u.models.Job;
import com.pantera.mobin.serv_u.models.JobInput;
import com.pantera.mobin.serv_u.models.JobsPage;
import com.pantera.mobin.serv_u.models.LoginInput;
import com.pantera.mobin.serv_u.models.Notifications;
import com.pantera.mobin.serv_u.models.SignUpInput;
import com.pantera.mobin.serv_u.models.UploadImage;
import com.pantera.mobin.serv_u.models.User;
import com.pantera.mobin.serv_u.utils.Constant;
import com.pantera.mobin.serv_u.utils.util;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mobin on 5/10/2017.
 */

public class ApiSERVU {
    private static String TAG = ApiSERVU.class.getSimpleName();
    //   private static String netError = "( No Internet Found )";

    private static String getError(String errorBody) {
        Gson gson = new GsonBuilder().create();
        ErrorPojo object = gson.fromJson(errorBody, ErrorPojo.class);
        //Log.w(TAG, "List " + object.errors);
        if (object != null) {
            if (object.errors != null)
                return object.errors.get(0);
            else return "";
        } else return "";
    }

    public static void addJob(final OnJobListener listener, JobInput jobInput) {
        Call<Job> call = RestClient.getAPI().addJob(Constant.token, Constant.uid, Constant.client, jobInput);
        call.enqueue(new Callback<Job>() {
            @Override
            public void onResponse(Call<Job> call, Response<Job> response) {
                if (response.code() < 210) {
                    listener.OnJobAdded(response.body());

                } else {
                    try {
                        String error = getError(response.errorBody().string());
                        listener.OnJobFailed(response.code() + error);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Job> call, Throwable t) {
                listener.OnJobFailed(t.getMessage());
            }
        });
    }

    public static void getCategories(final OnCategoriesListener listener, int page, String timeStamp) {
        Call<CategoryPage> call = RestClient.getAPI().getCategories(page, timeStamp);
        call.enqueue(new Callback<CategoryPage>() {
            @Override
            public void onResponse(Call<CategoryPage> call, Response<CategoryPage> response) {
                if (response.code() < 210) {
                    listener.OnCategoriesReceived(response.body());

                } else {
                    try {
                        String error = getError(response.errorBody().string());
                        listener.OnCategoriesFailure(response.code() + error);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<CategoryPage> call, Throwable t) {
                listener.OnCategoriesFailure(t.getMessage());
            }
        });

    }

    public static void getJobComments(final OnCommentsListener listener, int jobID, int page, String timeStamp) {
        Call<CommentsPage> call = RestClient.getAPI().getJobComments(jobID, page, timeStamp);
        call.enqueue(new Callback<CommentsPage>() {
            @Override
            public void onResponse(Call<CommentsPage> call, Response<CommentsPage> response) {
                if (response.code() < 210) {
                    listener.OnCommentsFetched(response.body());

                } else {
                    try {
                        String error = getError(response.errorBody().string());
                        listener.OnCommentsFailure(response.code() + error);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<CommentsPage> call, Throwable t) {
                listener.OnCommentsFailure(t.getMessage());
            }
        });
    }

    public static void getBids(final OnBidsListener listener, int jobID, int page, String timeStamp) {
        Call<BidsPage> call = RestClient.getAPI().getBids(jobID, page, timeStamp, Constant.token, Constant.uid, Constant.client);

        call.enqueue(new Callback<BidsPage>() {
            @Override
            public void onResponse(Call<BidsPage> call, Response<BidsPage> response) {
                if (response.code() < 210) {
                    listener.OnBidsReceived(response.body());

                } else {
                    try {
                        String error = getError(response.errorBody().string());
                        listener.OnBidsFetchFailed(response.code() + error);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<BidsPage> call, Throwable t) {
                listener.OnBidsFetchFailed(t.getMessage());
            }
        });
    }

    public static void logOut(Context context, final OnLogOutListener listener) {
        util.showProgressDialog(context, "Logging Out...", false);
        Call<ResponseBody> logOut = RestClient.getAPI().logOut(Constant.token, Constant.uid, Constant.client);

        logOut.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.code() < 210) {
                    listener.OnLogOutSuccess(true);

                } else {
                    try {
                        String error = getError(response.errorBody().string());
                        listener.OnLogOutFailed(response.code() + error);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                util.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                listener.OnLogOutFailed(t.getMessage());
                util.dismissProgressDialog();
            }
        });

    }

    public static void getProfile(final OnProfileListener listener) {
        Call<User> getProfile = RestClient.getAPI().getProfile(Constant.id);
        getProfile.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.code() < 210) {
                    listener.OnUserProfileReceived(response.body());

                } else {
                    try {
                        String error = getError(response.errorBody().string());
                        listener.OnUserProfileFailure(response.code() + error);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                listener.OnUserProfileFailure(t.getMessage());

            }
        });
    }

    public static void getNotifications(final OnNotificationsListener listener, int page, String timeStamp) {

        Call<Notifications> call = RestClient.getAPI().getNotifications(Constant.token, Constant.uid, Constant.client, page, timeStamp);
        call.enqueue(new Callback<Notifications>() {
            @Override
            public void onResponse(Call<Notifications> call, Response<Notifications> response) {
                if (response.code() < 210) {
                    listener.OnNotificationListReceived(response.body());

                } else {
                    try {
                        String error = getError(response.errorBody().string());
                        listener.OnNotificationsListFailure(error + response.code());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                util.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<Notifications> call, Throwable t) {

                listener.OnNotificationsListFailure(t.getMessage());

            }
        });


    }


    public static void uploadPic(final Context context, UploadImage image) {


        Call<Document> call = RestClient.getAPI().uploadProfilePic(Constant.token, Constant.uid, Constant.client, image);
        call.enqueue(new Callback<Document>() {
            @Override
            public void onResponse(Call<Document> call, Response<Document> response) {
                if (response.code() < 210) {

                    //      Toast.makeText(context, response.body().getFile_name() + " upload success", Toast.LENGTH_SHORT).show();
                    Constant.profile = response.body();
                    Toast.makeText(context, "Upload success", Toast.LENGTH_SHORT).show();


                } else {
                    try {
                        String error = getError(response.errorBody().string());
                        Toast.makeText(context, response.code() + error, Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                util.dismissProgressDialog();

            }

            @Override
            public void onFailure(Call<Document> call, Throwable t) {
                util.dismissProgressDialog();
                Toast.makeText(context, "Upload Failed.Try Again", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void getJobDetail(final OnJobDetailListener listener, String jobID) {
        Call<Job> call = RestClient.getAPI().getJobDetail(jobID);
        call.enqueue(new Callback<Job>() {
            @Override
            public void onResponse(Call<Job> call, Response<Job> response) {
                if (response.code() < 210) {
                    listener.OnJobDetailReceived(response.body());
                } else {
                    try {
                        listener.OnJobDetailFailure(response.code() + getError(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Job> call, Throwable t) {
                listener.OnJobDetailFailure(t.getMessage());
            }
        });
    }

    public static void getJobs(final OnJobListListener listener, int page, String timeStamp) {
        Call<JobsPage> call = RestClient.getAPI().getJobListings(page, timeStamp);
        call.enqueue(new Callback<JobsPage>() {
            @Override
            public void onResponse(Call<JobsPage> call, Response<JobsPage> response) {
                if (response.code() < 210) {
                    listener.OnJobsFetched(response.body());
                } else {
                    try {
                        listener.OnJobsFetchFailed(response.code() + getError(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JobsPage> call, Throwable t) {
                listener.OnJobsFetchFailed(t.getMessage());
            }
        });
    }

    public static void doSignUp(final OnSignUpListener listener, SignUpInput signUpInput) {
        Call<Auth> call = RestClient.getAPI().signUp(signUpInput);
        call.enqueue(new Callback<Auth>() {
            @Override
            public void onResponse(Call<Auth> call, Response<Auth> response) {
                if (response.code() < 210) {
                    listener.OnSignUPSuccess(response.body());
                } else {
                    try {
                        listener.OnSignUPFailed(response.code() + getError(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<Auth> call, Throwable t) {
                listener.OnSignUPFailed(t.getMessage());
            }
        });
    }

    public static void doLogin(final OnLoginListener listener, LoginInput loginInput) {
        Call<Auth> call = RestClient.getAPI().login(loginInput);
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                try {
//                    Log.v("login",response.body().string());
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//
//            }
//        });
        call.enqueue(new Callback<Auth>() {
            @Override
            public void onResponse(Call<Auth> call, Response<Auth> response) {
                if (response.code() < 210) {
                    listener.onLoginSuccess(response.body());
                } else {
                    try {
                        listener.onLoginFailed(response.code() + " " + getError(response.errorBody().string()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<Auth> call, Throwable t) {
                listener.onLoginFailed(t.getMessage());
            }
        });
    }

    private class ErrorPojo {
        @SerializedName("errors")
        @Expose
        List<String> errors = null;

    }

}
