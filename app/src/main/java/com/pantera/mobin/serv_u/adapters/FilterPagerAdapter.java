package com.pantera.mobin.serv_u.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.pantera.mobin.serv_u.R;
import com.pantera.mobin.serv_u.fragments.BudgetFragment;
import com.pantera.mobin.serv_u.fragments.CategoryFragment;
import com.pantera.mobin.serv_u.fragments.DistanceFragment;
import com.pantera.mobin.serv_u.fragments.SortByFragment;

/**
 * Created by Mobin on 5/25/2017.
 */

public class FilterPagerAdapter extends FragmentStatePagerAdapter {
    private int tabCount;
    private String[] title;

    public FilterPagerAdapter(Context context, FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;
        title = new String[]{context.getString(R.string.sort), context.getString(R.string.dis), context.getString(R.string.budget), context.getString(R.string.cat)};
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        switch (position) {
            case 0:
                fragment = new SortByFragment();
                break;
            case 1:
                fragment = new DistanceFragment();
                break;
            case 2:
                fragment = new BudgetFragment();
                break;
            case 3:
                fragment = new CategoryFragment();
                break;
            default:
                fragment = new SortByFragment();

        }

        return fragment;
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
