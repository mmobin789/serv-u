package com.pantera.mobin.serv_u.interfaces;

import com.pantera.mobin.serv_u.models.Auth;
import com.pantera.mobin.serv_u.models.BidsPage;
import com.pantera.mobin.serv_u.models.CategoryPage;
import com.pantera.mobin.serv_u.models.CommentsPage;
import com.pantera.mobin.serv_u.models.Document;
import com.pantera.mobin.serv_u.models.Job;
import com.pantera.mobin.serv_u.models.JobInput;
import com.pantera.mobin.serv_u.models.JobsPage;
import com.pantera.mobin.serv_u.models.LoginInput;
import com.pantera.mobin.serv_u.models.Notifications;
import com.pantera.mobin.serv_u.models.SignUpInput;
import com.pantera.mobin.serv_u.models.UploadImage;
import com.pantera.mobin.serv_u.models.User;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Mobin on 5/10/2017.
 */

public interface API {
    @POST("auth/sign_in")
    Call<Auth> login(@Body LoginInput loginInput);

    @POST("auth")
    Call<Auth> signUp(@Body SignUpInput signUpInput);

    @GET("jobs")
    Call<JobsPage> getJobListings(@Query("page") int page, @Query("timestamp") String timeStamp);

    @GET("jobs/{id}")
    Call<Job> getJobDetail(@Path("id") String id);

    @POST("documents")
    Call<Document> uploadProfilePic(
            @Header("token") String token, @Header("uid") String uid, @Header("client") String client,
            @Body UploadImage image);

    @GET("notifications")
    Call<Notifications> getNotifications(@Header("token") String token, @Header("uid") String uid, @Header("client") String client,
                                         @Query("page") int page, @Query("timestamp") String timeStamp);

    @GET("users/{id}")
    Call<User> getProfile(@Path("id") int id);

    @DELETE("auth/sign_out")
    Call<ResponseBody> logOut(@Header("token") String token, @Header("uid") String uid, @Header("client") String client);

    @GET("jobs/{job_id}/bids")
    Call<BidsPage> getBids(@Path("job_id") int jobID, @Query("page") int page, @Query("timestamp") String timeStamp,
                           @Header("token") String token, @Header("uid") String uid, @Header("client") String client);

    @GET("jobs/{job_id}/comments")
    Call<CommentsPage> getJobComments(@Path("job_id") int jobID, @Query("page") int page, @Query("timestamp") String timeStamp);

    @GET("categories")
    Call<CategoryPage> getCategories(@Query("page") int page, @Query("timestamp") String timeStamp);

    @POST("jobs")
    Call<Job> addJob(@Header("token") String token, @Header("uid") String uid, @Header("client") String client, @Body JobInput jobInput);


}
