package com.pantera.mobin.serv_u.models;

/**
 * Created by Mobin on 5/22/2017.
 */

public class Auth {
    private User user;
    private String token,client,uid;

    public User getUser() {
        return user;
    }

    public String getToken() {
        return token;
    }

    public String getClient() {
        return client;
    }

    public String getUid() {
        return uid;
    }
}
