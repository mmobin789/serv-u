package com.pantera.mobin.serv_u.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.pantera.mobin.serv_u.R;
import com.pantera.mobin.serv_u.SERVU;

public class Splash extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        delay(1);

    }

    private void delay(long seconds) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
              checkLogin();
            }
        }, seconds * 1000);
    }

    private void checkLogin() {
        Intent i = new Intent();
        if (SERVU.hasLoginPrefs()) {
            i.setClass(this, MainActivity.class);
        } else {
            i.setClass(this, LoginActivity.class);
        }
        finish();
        startActivity(i);
    }

    @Override
    void initViews() {

    }

    @Override
    void setListeners() {

    }


}
