package com.pantera.mobin.serv_u.models;

/**
 * Created by Mobin on 5/16/2017.
 */

public class Category {

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getCreated_at() {
        return created_at;
    }

    public Document getThumb() {
        return thumb;
    }

   private int id;
   private String title, created_at;
   private Document thumb;
   private boolean checked;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
