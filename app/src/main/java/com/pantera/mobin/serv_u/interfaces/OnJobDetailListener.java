package com.pantera.mobin.serv_u.interfaces;

import com.pantera.mobin.serv_u.models.Job;

/**
 * Created by Mobin on 5/16/2017.
 */

public interface OnJobDetailListener {

    void OnJobDetailReceived(Job job);
    void OnJobDetailFailure(String error);
}
