package com.pantera.mobin.serv_u.interfaces;

import com.pantera.mobin.serv_u.models.CommentsPage;

/**
 * Created by Mobin on 5/24/2017.
 */

public interface OnCommentsListener {

    void OnCommentsFetched(CommentsPage commentsPage);

    void OnCommentsFailure(String error);
}
