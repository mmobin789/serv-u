package com.pantera.mobin.serv_u.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.pantera.mobin.serv_u.R;
import com.pantera.mobin.serv_u.models.Bid;
import com.pantera.mobin.serv_u.utils.CircleTranform;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Mobin on 5/22/2017.
 */

public class BidsAdapter extends RecyclerView.Adapter<BidsAdapter.BidsViewHolder> {
    private List<Bid> bids;

    public BidsAdapter(List<Bid> bids) {
        this.bids = bids;
    }

    @Override
    public BidsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BidsViewHolder(View.inflate(parent.getContext(), R.layout.bids_adapter, null));
    }

    @Override
    public void onBindViewHolder(BidsViewHolder holder, int position) {

        Bid bid = bids.get(position);

        holder.price.setText("AED "+bid.getBudget()+"");
        holder.userName.setText(bid.getUser().getName());
        holder.des.setText(bid.getDescription());
       // holder.ratingBar.setRating();
        Picasso.with(holder.itemView.getContext()).load(bid.getUser().getProfile_pic().getUrl()).transform(new CircleTranform()).into(holder.userAvatar);


    }

    @Override
    public int getItemCount() {
        return bids.size();
    }

    class BidsViewHolder extends RecyclerView.ViewHolder {
        TextView price,userName,des;
        RatingBar ratingBar;
        ImageView userAvatar;

         BidsViewHolder(View itemView) {
            super(itemView);

             price = (TextView) itemView.findViewById(R.id.price);
             ratingBar = (RatingBar) itemView.findViewById(R.id.ratingBar);
             userAvatar = (ImageView) itemView.findViewById(R.id.userPic);
             userName = (TextView) itemView.findViewById(R.id.name);
             des = (TextView) itemView.findViewById(R.id.des);

        }
    }
}
