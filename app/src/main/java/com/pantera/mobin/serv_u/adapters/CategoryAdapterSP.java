package com.pantera.mobin.serv_u.adapters;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pantera.mobin.serv_u.R;
import com.pantera.mobin.serv_u.models.Category;

import java.util.List;

/**
 * Created by Mobin on 6/5/2017.
 */

public class CategoryAdapterSP extends RecyclerView.Adapter<CategoryAdapterSP.CategoryViewHolder> {
    private List<Category> list;

    public CategoryAdapterSP(List<Category> list) {
        this.list = list;
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new CategoryViewHolder(View.inflate(viewGroup.getContext(), R.layout.category_adapter, null));
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder categoryViewHolder, int i) {
        Category category = list.get(i);
        categoryViewHolder.name.setText(category.getTitle());

        if (category.isChecked()) {
            categoryViewHolder.status.setImageResource(R.drawable.tick);
        } else {
            categoryViewHolder.status.setImageDrawable(new ColorDrawable(Color.TRANSPARENT));
        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class CategoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name;
        ImageView status;

        CategoryViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            status = (ImageView) itemView.findViewById(R.id.status);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Category category = list.get(getAdapterPosition());
            if (category.isChecked()) {
                category.setChecked(false);
            } else {
                category.setChecked(true);
            }
            notifyItemChanged(getAdapterPosition());
        }
    }
}
