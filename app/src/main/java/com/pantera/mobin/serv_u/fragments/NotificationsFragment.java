package com.pantera.mobin.serv_u.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.paginate.Paginate;
import com.pantera.mobin.serv_u.R;
import com.pantera.mobin.serv_u.activities.MainActivity;
import com.pantera.mobin.serv_u.adapters.NotificationsAdapter;
import com.pantera.mobin.serv_u.api.ApiSERVU;
import com.pantera.mobin.serv_u.interfaces.OnNotificationsListener;
import com.pantera.mobin.serv_u.models.Notification;
import com.pantera.mobin.serv_u.models.Notifications;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link NotificationsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NotificationsFragment extends Fragment implements OnNotificationsListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    RecyclerView recyclerView;
    Paginate paginate;
    NotificationsAdapter adapter;
    List<Notification> notificationList = new ArrayList<>();
    boolean isLoading = false;
    boolean hasLoadedAllItems = false;
    int pageNO;
    String timeStamp;


    public NotificationsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment NotificationsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NotificationsFragment newInstance() {
        NotificationsFragment fragment = new NotificationsFragment();
        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
////            mParam1 = getArguments().getString(ARG_PARAM1);
////            mParam2 = getArguments().getString(ARG_PARAM2);
//        }
    }

    Paginate.Callbacks callbacks = new Paginate.Callbacks() {
        @Override
        public void onLoadMore() {
            isLoading = true;
            getNotifications();
        }

        @Override
        public boolean isLoading() {
            return isLoading;
        }

        @Override
        public boolean hasLoadedAllItems() {
            return hasLoadedAllItems;
        }
    };

    private void getNotifications() {

        ApiSERVU.getNotifications(this, getPageNo(), getTimeStamp());
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        ((MainActivity) getContext()).notificationsToolbar();
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new NotificationsAdapter(notificationList);
        recyclerView.setAdapter(adapter);
        paginate = Paginate.with(recyclerView, callbacks).setLoadingTriggerThreshold(1)
                .addLoadingListItem(true).build();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notifications, container, false);
    }

    @Override
    public void OnNotificationListReceived(Notifications notifications) {

        isLoading = false;
        pageNO = notifications.getPage();
        timeStamp = notifications.getTimestamp();

        if (pageNO == 1) {
            if (notificationList.size() == 0) {
                hasLoadedAllItems = !notifications.isMore_available();
                notificationList.addAll(notifications.getNotifications());

                Toast.makeText(getContext(), "No Notifications Found", Toast.LENGTH_SHORT).show();
            }
            adapter.notifyItemRangeChanged(0, notificationList.size());


        } else {
            int lastIndex = notificationList.size() - 1;
            if (lastIndex != -1) {
                adapter.notifyItemRangeChanged(lastIndex + 1, notificationList.size());
            } else {
                adapter.notifyItemRangeChanged(0, notificationList.size());
            }

        }


    }

    @Override
    public void OnNotificationsListFailure(String error) {
        ((MainActivity) getActivity()).showErrorLayout(error);
        isLoading = false;
        hasLoadedAllItems = true;
    }

    private int getPageNo() {
        if (pageNO > 0)
            pageNO++;
        return pageNO;
    }

    private String getTimeStamp() {
        if (pageNO == 1)
            return null;
        else
            return timeStamp;
    }
}
