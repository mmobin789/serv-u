package com.pantera.mobin.serv_u.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker;
import com.pantera.mobin.serv_u.R;
import com.pantera.mobin.serv_u.interfaces.OnJobListener;
import com.pantera.mobin.serv_u.models.Job;
import com.pantera.mobin.serv_u.models.JobInput;

import java.util.Date;

public class NewJobActivity extends BaseActivity implements View.OnClickListener, SingleDateAndTimePicker.Listener, OnJobListener {

    SingleDateAndTimePicker singleDateAndTimePicker;
    LinearLayout scheduledTab, contractTab;
    TextView urgent, contract, scheduled, date;
    EditText jobTitle, des, price, addNotes, partNo, city, address, country;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_job);
        initViews();
        setListeners();
    }

    @Override
    void initViews() {
        setToolbar();
        jobTitle = (EditText) findViewById(R.id.jobTitle);
        des = (EditText) findViewById(R.id.des);
        price = (EditText) findViewById(R.id.budget);
        addNotes = (EditText) findViewById(R.id.addNotes);
        partNo = (EditText) findViewById(R.id.partNo);
        city = (EditText) findViewById(R.id.city);
        country = (EditText) findViewById(R.id.country);
        address = (EditText) findViewById(R.id.address);
        urgent = (TextView) findViewById(R.id.urgent);
        contract = (TextView) findViewById(R.id.contract);
        scheduled = (TextView) findViewById(R.id.scheduled);
        contractTab = (LinearLayout) findViewById(R.id.contractType);
        date = (TextView) findViewById(R.id.date);
        scheduledTab = (LinearLayout) findViewById(R.id.scheduledTab);
        singleDateAndTimePicker = (SingleDateAndTimePicker) findViewById(R.id.dateTimePicker);
        initDateTimePicker();

    }

    private void initDateTimePicker() {
        // singleDateAndTimePicker.setMinDate(Calendar.getInstance().getTime());
        singleDateAndTimePicker.setMustBeOnFuture(true);
        singleDateAndTimePicker.setIsAmPm(false);
        singleDateAndTimePicker.setVisibleItemCount(8);
        singleDateAndTimePicker.setStepMinutes(1);
        View dtSelector = singleDateAndTimePicker.findViewById(R.id.dtSelector);
        dtSelector.setBackgroundResource(R.drawable.border);

    }

    @Override
    void setListeners() {
        urgent.setOnClickListener(this);
        contract.setOnClickListener(this);
        scheduled.setOnClickListener(this);
        singleDateAndTimePicker.setListener(this);
    }

    private void setToolbar() {
        showToolBar(getString(R.string.jobDetails), false);
        TextView done = (TextView) findViewById(R.id.editTxt);
        done.setVisibility(View.VISIBLE);
        done.setText(getString(R.string.done));
        done.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.editText:
                addJob();
                break;
            case R.id.scheduled:
                jobScheduled(true);
                jobContract(false);
                jobUrgent(false);
                break;
            case R.id.urgent:
                jobUrgent(true);
                jobScheduled(false);
                jobContract(false);
                break;
            case R.id.contract:
                jobContract(true);
                jobScheduled(false);
                jobUrgent(false);
                break;


        }
    }

    private void addJob() {
        JobInput data = new JobInput();
        String title = jobTitle.getText().toString().trim();
        String desc = des.getText().toString().trim();
        String addNote = addNotes.getText().toString().trim();
        String budget = price.getText().toString().trim();
        String partNum = partNo.getText().toString().trim();
        String countryTxt = country.getText().toString().trim();
        String cityTxt = city.getText().toString().trim();
        String addressTxt = address.getText().toString().trim();

        if (TextUtils.isEmpty(title))
            jobTitle.setError("Title Required");
        if (TextUtils.isEmpty(desc))
            des.setError("Required");
    }

    private void jobContract(boolean isContract) {
        if (isContract) {
            contract.setTextColor(Color.WHITE);
            contract.setBackgroundResource(R.drawable.right_round_clicked_contract);
            scheduledTab.setVisibility(View.VISIBLE);
            date.setText(getString(R.string.first));
            contractTab.setVisibility(View.VISIBLE);
        } else {
            contract.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            contract.setBackgroundResource(R.drawable.single_leftside_border);
        }


    }

    private void jobScheduled(boolean isScheduled) {
        if (isScheduled) {
            scheduled.setTextColor(Color.WHITE);
            scheduled.setBackgroundColor(ContextCompat.getColor(this, R.color.btn_border));
            scheduledTab.setVisibility(View.VISIBLE);
            contractTab.setVisibility(View.GONE);
            date.setText(getString(R.string.date));

        } else {
            scheduled.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            scheduled.setBackgroundColor(Color.TRANSPARENT);
        }


    }

    private void jobUrgent(boolean isUrgent) {
        if (isUrgent) {
            urgent.setTextColor(ContextCompat.getColor(this, R.color.white));
            urgent.setBackgroundResource(R.drawable.left_round_clicked_urgent);
            scheduledTab.setVisibility(View.GONE);
        } else {
            urgent.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            urgent.setBackgroundResource(R.drawable.single_rightside_border);
        }

    }

    @Override
    public void onDateChanged(String displayed, Date date) {
        Toast.makeText(this, displayed, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void OnJobAdded(Job job) {

    }

    @Override
    public void OnJobFailed(String error) {

    }
}
