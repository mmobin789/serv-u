package com.pantera.mobin.serv_u.models;

/**
 * Created by Mobin on 5/22/2017.
 */

public class LoginInput {
    private String email,password;

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
