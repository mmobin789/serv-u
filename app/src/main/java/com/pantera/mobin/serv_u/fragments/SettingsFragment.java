package com.pantera.mobin.serv_u.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pantera.mobin.serv_u.R;
import com.pantera.mobin.serv_u.activities.MainActivity;
import com.pantera.mobin.serv_u.adapters.SettingsAdapter;
import com.pantera.mobin.serv_u.models.Settings;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SettingsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SettingsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    RecyclerView recyclerView;

    // TODO: Rename and change types of parameters


    public SettingsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.

     * @return A new instance of fragment SettingsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SettingsFragment newInstance() {
        //  Bundle args = new Bundle();
      //  args.putString(ARG_PARAM1, param1);
      //  args.putString(ARG_PARAM2, param2);
      ///  fragment.setArguments(args);
        return new SettingsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
//        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        ((MainActivity)getContext()).settingsToolbar();
        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        ArrayList<Settings> settingList = new ArrayList<>();
        settingList.add(new Settings("Email Alerts", 0));
        settingList.add(new Settings("New Related Job", true, 1));
        settingList.add(new Settings("New Review", true, 1));
        settingList.add(new Settings("New Message", false, 1));
        settingList.add(new Settings("Push Notifications", 0));
        settingList.add(new Settings("New Related Job", true, 1));
        settingList.add(new Settings("New Review", true, 1));
        settingList.add(new Settings("New Message", false, 1));


        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new SettingsAdapter(settingList));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

}
