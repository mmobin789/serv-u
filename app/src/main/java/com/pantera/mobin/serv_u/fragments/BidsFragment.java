package com.pantera.mobin.serv_u.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.paginate.Paginate;
import com.pantera.mobin.serv_u.R;
import com.pantera.mobin.serv_u.activities.JobDetailActivity;
import com.pantera.mobin.serv_u.adapters.BidsAdapter;
import com.pantera.mobin.serv_u.api.ApiSERVU;
import com.pantera.mobin.serv_u.interfaces.OnBidsListener;
import com.pantera.mobin.serv_u.models.Bid;
import com.pantera.mobin.serv_u.models.BidsPage;
import com.pantera.mobin.serv_u.utils.Constant;

import java.util.ArrayList;
import java.util.List;


public class BidsFragment extends Fragment implements OnBidsListener {
    // TODO: Rename parameter arguments, choose names that match

    private int jobID;
    boolean isLoading = false;
    boolean hasLoadedAllItems = false;
    int pageNO;
    Paginate paginate;
    String timeStamp;
    RecyclerView recyclerView;
    BidsAdapter adapter;
    List<Bid> bids = new ArrayList<>();

    public BidsFragment() {
        // Required empty public constructor
    }

    Paginate.Callbacks callbacks = new Paginate.Callbacks() {
        @Override
        public void onLoadMore() {
            isLoading = true;
            getBids();

        }

        @Override
        public boolean isLoading() {
            return isLoading;
        }

        @Override
        public boolean hasLoadedAllItems() {
            return hasLoadedAllItems;
        }
    };

    private void getBids() {

        ApiSERVU.getBids(this, jobID, getPageNo(), getTimeStamp());

    }

    private int getPageNo() {
        if (pageNO > 0)
            pageNO++;
        return pageNO;
    }

    private String getTimeStamp() {
        if (pageNO == 1)
            return null;
        else
            return timeStamp;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new BidsAdapter(bids);
        recyclerView.setAdapter(adapter);
        paginate = Paginate.with(recyclerView, callbacks).setLoadingTriggerThreshold(1).addLoadingListItem(true)
                .build();
        //     util.showProgressDialog(getContext(), "Loading Bids...", false);

    }

    // TODO: Rename and change types and number of parameters
    public static BidsFragment newInstance(int jobID) {
        BidsFragment fragment = new BidsFragment();
        Bundle args = new Bundle();
        args.putInt(Constant.key_jobID, jobID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            jobID = getArguments().getInt(Constant.key_jobID);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bids, container, false);
    }

    @Override
    public void OnBidsReceived(BidsPage bidsPage) {
        isLoading = false;
        pageNO = bidsPage.getPage();
        timeStamp = bidsPage.getTimeStamp();
        hasLoadedAllItems = !bidsPage.isMore_available();
        bids.addAll(bidsPage.getBids());


        if (pageNO == 1) {
            if (bids.size() == 0) {
                ((JobDetailActivity) getContext()).showErrorLayout(getView(), "No Bids Found");
                hasLoadedAllItems = true;

            }
                adapter.notifyItemRangeChanged(0, bids.size());
                    //    util.dismissProgressDialog();

        } else {
            int lastIndex = bids.size() - 1;
            if (lastIndex != -1) {
                adapter.notifyItemRangeChanged(lastIndex + 1, bids.size());
            } else {
                adapter.notifyItemRangeChanged(0, bids.size());
            }

        }

    }

    @Override
    public void OnBidsFetchFailed(String error) {
        ((JobDetailActivity) getContext()).showErrorLayout(getView(), error);
        //   util.dismissProgressDialog();
        isLoading = false;
        hasLoadedAllItems = true;
    }

    // TODO: Rename method, update argument and hook method into UI event


}
